/*  sdcard.h - SD Card diagnose codes and command function signatures
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SDCARD_H
#define SDCARD_H

#include <inttypes.h>

/// Collects all allowed return error codes of commands.
enum SDCARD_CMD_RETURN_CODE {
	/// Command successful.
	SDCARD_CMD_RET_SUCCESS = 0x00,
	/// Unknown command.
	SDCARD_CMD_RET_UNKNOWNCMD = 0x01,
	/// Incorrct parameters.
	SDCARD_CMD_RET_INCORRPARAMS = 0x02,
	/// Operation failed.
	/// Also thrown if the SD Card is write-protected.
	SDCARD_CMD_RET_OPFAILED = 0x03,
	/// Reached end of file/folder list. This is not an error.
	SDCARD_CMD_RET_EOLIST = 0x04,
	/// Media does not initialize.
	SDCARD_CMD_RET_NOINIT = 0x10,
	/// Initialize media failed.
	/// Also thrown if the card detect signal is high or floating.
	SDCARD_CMD_RET_INITFAILED = 0x11,
	/// File/folder does not exist.
	SDCARD_CMD_RET_NOTEXIST = 0x20,
	/// Failed to open file.
	SDCARD_CMD_RET_OPENFAILED = 0x21,
	/// Seek only runs on files open for read.
	SDCARD_CMD_RET_SEEKNOTREAD = 0x22,
	/// Seek value can only be within file size.
	SDCARD_CMD_RET_SEEKNOTFILESIZE = 0x23,
	/// File name cannot be zero.
	SDCARD_CMD_RET_ZEROFILENAME = 0x24,
	/// File name has forbidden character.
	SDCARD_CMD_RET_FORBIDDENCHAR = 0x25,
	/// File/folder name already exists.
	SDCARD_CMD_RET_TARGETEXISTS = 0x26,
	/// Invalid handle.
	SDCARD_CMD_RET_INVHND = 0x30,
	/// Handle source does not open.
	SDCARD_CMD_RET_HNDSRCOPEN = 0x31,
	/// Handle destination does not open.
	SDCARD_CMD_RET_HNDDSTOPEN = 0x32,
	/// Handle source requires file open for read mode.
	SDCARD_CMD_RET_HNDSRCNOREAD = 0x33,
	/// Handle destination requires file open for write or append mode.
	SDCARD_CMD_RET_HNDDSTNOWRITEAPPEND = 0x34,
	/// No more handle available.
	SDCARD_CMD_RET_NOHND = 0x35,
	/// Handle does not open.
	SDCARD_CMD_RET_HNDNOOPEN = 0x36,
	/// Handle is already in use.
	SDCARD_CMD_RET_HNDINUSE = 0x37,
	/// Open file mode invalid.
	SDCARD_CMD_RET_OPENMODEINVALID = 0x38,
	/// Handle requires write or append mode.
	SDCARD_CMD_RET_HNDNOWRITEAPPEND = 0x39,
	/// Handle requires read mode.
	SDCARD_CMD_RET_HNDNOREAD = 0x3a,
	/// The system is busy.
	SDCARD_CMD_RET_SYSBUSY = 0x40,
	/// Command is supported with SPI interface only.
	SDCARD_CMD_RET_SPIONLY = 0x41,
	/// Boot loader indication code.
	SDCARD_CMD_RET_BOOTLOADER = 0xff
};

enum SDCARD_DIAG_BASE {
	SDCARD_DIAG_PROGRESS = 1,
	SDCARD_DIAG_BUSY = 2,
	SDCARD_DIAG_PARTIAL = 4,
	SDCARD_DIAG_COMPLETED_SUCCESS = 8,
	SDCARD_DIAG_COMPLETED_FAIL = 16,
};

enum SDCARD_DIAG_COMPLETED_FAIL_REASON {
	/// Command return code must be enclosed by '!' and '\n'.
	REASON_RETCODE_DELIM,
	/// A retrun code different from #SDCARD_CMD_RET_SUCCESS was returned.
	REASON_RETCODE_VALUE,
	/// Payload data in a reply must be enclosed by '$' and '\n'.
	REASON_DATA_DELIM,
	/// Expected bytes to be written and actuall written bytes do not match.
	REASON_FASTWRITE_NUMBYTES_MISMATCH,
	/// Either no SD Card detected, or SD Card is write protected.
	REASON_READSTATUS_FLAGERROR
};

void sdcard_open_initsm(const uint8_t packed_name);
enum SDCARD_DIAG_BASE sdcard_open(void);

void sdcard_close_initsm(void);
enum SDCARD_DIAG_BASE sdcard_close(void);

void sdcard_fastwrite_initsm(const uint8_t * const ch_data,
			     const uint8_t * const wd_chunk,
			     uint8_t * const rd_chunk);
enum SDCARD_DIAG_BASE sdcard_fastwrite(const uint8_t cancel);

void sdcard_init_initsm(void);
enum SDCARD_DIAG_BASE sdcard_init(void);

void sdcard_readstatus_initsm(uint8_t * const status);
enum SDCARD_DIAG_BASE sdcard_readstatus(void);

void sdcard_freesize_initsm(uint64_t * const free);
enum SDCARD_DIAG_BASE sdcard_freesize(void);

void sdcard_testmedia_initsm(uint32_t * const wmillis,
			     uint32_t * const rmillis);
enum SDCARD_DIAG_BASE sdcard_testmedia(void);

void sdcard_format_initsm(void);
enum SDCARD_DIAG_BASE sdcard_format(void);

#endif				/* SDCARD_H */
