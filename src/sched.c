/*  sched.c - scheduler and scheduler setup
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/interrupt.h>
#include "sched.h"
#include "tasking.h"
#include "config.h"

/// Scheduling plan, implemented as function pointer array.
///
/// Empty slots <b>MUST NOT BE NULL<b>. An empty slot in the scheduling plan
/// must be assigned to task_dummy, because the scheduler ISR directly jumps to
/// the address pointed to by the array elements.
static void (*scheduling_map[BC_NUM][SLOT_NUM]) (void)
    __attribute__ ((section(".noinit")));

/// The current basic cycle (row index) the scheduler is in.
static uint8_t basic_cycle;
/// The current slot number (column index) the scheduler is in.
static uint8_t current_slot;

/// Initialize the scheduling map and prepare the scheduling timer.
void sched_init(void)
{
	// preset task map -- entries MUST NOT BE NULL!
	for (uint8_t bc = 0; bc < BC_NUM; ++bc) {
		for (uint8_t s = 0; s < SLOT_NUM; ++s) {
			scheduling_map[bc][s] = pcb_task_dummy.handler;
		}
	}
	// add BC-regular tasks
	for (uint8_t bc = 0; bc < BC_NUM; ++bc) {
		scheduling_map[bc][0] = pcb_task_accel_fiber.handler;
		scheduling_map[bc][1] = pcb_task_accel_adxl.handler;
		scheduling_map[bc][2] = pcb_task_sdcard.handler;
		scheduling_map[bc][3] = pcb_task_accel_fiber.handler;
		scheduling_map[bc][4] = pcb_task_sdcard.handler;
		scheduling_map[bc][5] = pcb_task_telecommand.handler;
		scheduling_map[bc][6] = pcb_task_accel_fiber.handler;
		scheduling_map[bc][7] = pcb_task_sdcard.handler;
		scheduling_map[bc][8] = pcb_task_sdcard.handler;
		scheduling_map[bc][9] = pcb_task_accel_fiber.handler;
		scheduling_map[bc][10] = pcb_task_sdcard.handler;
	}

	// add sporadic tasks
	// scheduling_map[0][11] is unallocated/free
	scheduling_map[1][11] = pcb_task_sample.handler;
	scheduling_map[2][11] = pcb_task_sample.handler;
	scheduling_map[3][11] = pcb_task_sample.handler;
	// scheduling_map[4][11] is unallocated/free
	scheduling_map[5][11] = pcb_task_hires_temperature.handler;
	scheduling_map[6][11] = pcb_task_lores_temperature.handler;
	// scheduling_map[7][11] is unallocated/free
	// scheduling_map[8][11] is unallocated/free
	for (uint8_t bc = 9; bc <= 32; ++bc) {
		scheduling_map[bc][11] = pcb_task_telemetry.handler;
	}
	// scheduling_map[33][11] is unallocated/free
	scheduling_map[34][11] = pcb_task_telemetry.handler;
	scheduling_map[35][11] = pcb_task_loader.handler;

	// configure scheduling timer interrupt
	SCHED_TIMER.PERBUF = SCHED_PERIOD;
	SCHED_TIMER.PER = SCHED_PERIOD;
	SCHED_TIMER.CCABUF = SCHED_COMPAREVAL;
	SCHED_TIMER.CCA = SCHED_COMPAREVAL;
	SCHED_TIMER.CTRLB = SCHED_TIMER_CCAEN_bm | TC_WGMODE_SINGLESLOPE_gc;
	SCHED_TIMER.INTCTRLB = TC_CCAINTLVL_LO_gc | TC_CCAINTLVL_MED_gc
	    | TC_CCAINTLVL_HI_gc;
	PMIC.CTRL = PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
}

/// Starts scheduler timer and resets basic cycle and slot counters.
void sched_start(void)
{
	basic_cycle = 0;
	current_slot = 0;
	SCHED_TIMER.CTRLA = TC_CLKSEL_DIV1_gc;
}

/// Scheduler ISR to load the task for the current slot and basic cycle.
ISR(SCHED_TIMER_CCA_vect)
{
	uint8_t bc = basic_cycle;
	uint8_t slot = current_slot;

	// OPTIMIZED CODE
	// DO NOT TOUCH UNLESS YOU NOW EXACTLY WHAT YOU DO!
	//
	// This does the same as
	//      void (*const task_handler)(void) = *scheduling_map[bc][slot];
	// using the following steps to calculate the call target:
	// (1) element index:          bc * 12 + slot
	// (2) index to word address:  elem_idx * 2
	// (3) add base address:       elem_word + base_offset
	void (*task_handler) (void);
	uint8_t tmp;
	asm volatile ("ldi  %[tmp], %[slotnum] ;                   \n\t"
		      "mul  %[bc], %[tmp]      ;                   \n\t"
		      "movw r30, r0            ;                   \n\t"
		      "clr  r1                 ; Z <- bc * 12      \n\t"
		      "add  r30, %[slot]       ;                   \n\t"
		      "adc  r31, __zero_reg__  ; Z <- Z + slot     \n\t"
		      "add  r30, r30           ;                   \n\t"
		      "adc  r31, r31           ; Z <- Z * 2        \n\t"
		      "subi r30, lo8(-(scheduling_map))"
		      "                        ;                   \n\t"
		      "sbci r31, hi8(-(scheduling_map))"
		      "                        ; Z <- Z - (-base)  \n\t"
		      "ld   %[tmp], Z+         ;                   \n\t"
		      "ld   r31, Z             ;                   \n\t"
		      "mov  r30, %[tmp]        ; Z <- (Z)          \n\t"
		      : [tskhand] "=&z" (task_handler), [tmp] "=&d" (tmp)
		      : [slotnum] "i" (SLOT_NUM),
			[slot] "d" (slot),
			[bc] "d" (bc)
		      : "r0");

	++slot;
	if (slot >= SLOT_NUM) {
		slot = 0;
		++bc;
	}
	if (bc >= BC_NUM) {
		bc = 0;
	}
	basic_cycle = bc;
	current_slot = slot;

	(*task_handler) ();
}
