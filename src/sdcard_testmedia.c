/*  sdcard_testmedia.c - command to selftest the SD Card
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sdcard.h"
#include "sdcard_transactions.h"
#include "sdcard_helper.h"

/// The state of the internal state machine.
static enum {
	CLEAR_FIFO,
	CMD,
	RET,
	CHECK_RET,
	MILLISRET,
	CHECK_MILLISRET
} state;

static void cmd_callback(void);
static void busy_callback(void);
static void check_ret_success_callback(void);
static enum SDCARD_DIAG_BASE state_testmedia_checkmillisret(void);

/// ALFAT command to read/write test data to the SD Card.
static const char ALFAT_TESTMEDIA_CMD[] = "E M:>00400000\n";
/// The reference to the target location to save the milliseconds for write.
static uint32_t *write_millis;
/// The reference to the target location to save the milliseconds for read.
static uint32_t *read_millis;

static uint8_t header_pos;
static uint8_t total_received_bytes;

void sdcard_testmedia_initsm(uint32_t * const wmillis, uint32_t * const rmillis)
{
	write_millis = wmillis;
	read_millis = rmillis;
	header_pos = 0;
	total_received_bytes = 0;
	state = CLEAR_FIFO;
}

/// Send "E" command to SD Card.
enum SDCARD_DIAG_BASE sdcard_testmedia(void)
{
	if (state == CLEAR_FIFO)
		return sdcard_helper_clear_fifo(&header_pos, &state, CMD);
	if (state == CMD)
		return sdcard_helper_cmd(&header_pos, ALFAT_TESTMEDIA_CMD,
					 sizeof(ALFAT_TESTMEDIA_CMD) - 1,
					 &busy_callback, &cmd_callback);
	if (state == RET)
		return sdcard_helper_receive(&header_pos, &state,
					     &total_received_bytes, 4,
					     CHECK_RET);
	if (state == MILLISRET)
		return sdcard_helper_receive(&header_pos, &state,
					     &total_received_bytes, 24,
					     CHECK_MILLISRET);
	if (state == CHECK_RET)
		return sdcard_helper_check_cmderrno(0,
						    &check_ret_success_callback,
						    SDCARD_DIAG_PROGRESS);
	return state_testmedia_checkmillisret();
}

static void cmd_callback(void)
{
	total_received_bytes = 0;
	state = RET;
}

static void busy_callback(void)
{
	header_pos = 0;
	total_received_bytes = 0;
	state = CLEAR_FIFO;
}

static void check_ret_success_callback(void)
{
	header_pos = 0;
	total_received_bytes = 0;
	state = MILLISRET;
}

static enum SDCARD_DIAG_BASE state_testmedia_checkmillisret(void)
{
	const char *const buffer = rx_buffer;
	if (!(buffer[20] == '!' && buffer[23] == '\n')) {
		sys_health.sdcard.reason = REASON_RETCODE_DELIM;
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	const uint8_t retcode = hex2int(buffer[21]) << 4 | hex2int(buffer[22]);
	if (retcode != SDCARD_CMD_RET_SUCCESS) {
		sys_health.sdcard.reason = REASON_RETCODE_VALUE;
		sys_health.sdcard.retcode = retcode;
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	if (!(buffer[0] == '$' && buffer[9] == '\n'
	      && buffer[10] == '$' && buffer[19] == '\n')) {
		sys_health.sdcard.reason = REASON_DATA_DELIM;
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	uint32_t millis = 0;
	// caution: uncommon loop boundaries
	for (uint8_t i = 1; i <= 8; ++i) {
		millis = (millis << 4) | hex2int(buffer[i]);
	}
	*write_millis = millis;

	millis = 0;
	// caution: uncommon loop boundaries
	for (uint8_t i = 11; i <= 18; ++i) {
		millis = (millis << 4) | hex2int(buffer[i]);
	}
	*read_millis = millis;

	return SDCARD_DIAG_COMPLETED_SUCCESS;
}
