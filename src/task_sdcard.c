/*  task_sdcard.c - task to handle commands and states while interfacing
 *                  to the SD Card
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tasking.h"
#include "system.h"
#include "filename.h"
#include "sdcard.h"

static inline void handler_powerup(const uint8_t ts);
static inline void handler_ready(void);
static inline void handler_file(const uint8_t ts);
static inline void handler_openfile(void);
static inline void handler_writefile(void);
static inline void handler_closefile(void);
static inline void handler_selftest(const uint8_t ts);
static inline void handler_selftest_readstatus(void);
static inline void handler_selftest_freesize(void);
static inline void handler_selftest_testmedia(void);
static inline void handler_format(void);

// the order of items is relevant!
static enum {
	/// Pull the RESET pin of the ALFAT to low, to force a hard reset.
	POWERUP_RESET_SDCARD_CONTROLLER_ENTRY = 1,
	/// Check if some time passed, and pull the RESET pin of the ALFAT
	/// to high, to allow bootup of the controller.
	POWERUP_RESET_SDCARD_CONTROLLER_DOEXIT,
	/// Wait some time for the ALFAT controller to reset and initialize
	/// itself.
	POWERUP_WAIT_SDCARD_CONTROLLER_INIT_ENTRY,
	/// Check whether the time passed that the controller typically needs
	/// for its initialization.
	POWERUP_WAIT_SDCARD_CONTROLLER_INIT_DOEXIT,
	/// Send the init command to the ALFAT controller, to initialize the
	/// SD Card.
	POWERUP_SDCARD_INIT_CMD,
	STOPPED,
	READY,
	FILE_OPEN,
	FILE_FASTWRITE,
	FILE_CLOSE,
	FORMATTING_ENTRY,
	FORMATTING_DOEXIT,
	SELFTEST_ENTRY,
	SELFTEST_READSTATUS,
	SELFTEST_FREESIZE,
	SELFTEST_TESTMEDIA
} task_state;

static enum {
	UNKNOWN,
	HEALTHY,
	DEGRADED
} health_state;

static uint8_t stop_req_flag;
static uint8_t selftest_req_flag;
static uint8_t format_req_flag;

void _task_sdcard_init_por(void)
{
	stop_req_flag = 0;
	selftest_req_flag = 0;
	format_req_flag = 0;
	task_state = POWERUP_RESET_SDCARD_CONTROLLER_ENTRY;
	health_state = UNKNOWN;
}

uint8_t _task_sdcard_is_stopped(void)
{
	return STOPPED == task_state;
}

void _task_sdcard_stop(void)
{
	if (STOPPED != task_state)
		stop_req_flag = 1;
}

void _task_sdcard_resume(void)
{
	if (STOPPED == task_state && DEGRADED != health_state &&
	    !stop_req_flag && !format_req_flag && !selftest_req_flag)
		task_state = READY;
}

uint8_t _task_sdcard_is_selftest(void)
{
	const uint8_t ts = task_state;
	return ts >= SELFTEST_ENTRY && ts <= SELFTEST_TESTMEDIA;
}

void _task_sdcard_selftest(void)
{
	selftest_req_flag = 1;
}

uint8_t _task_sdcard_is_health_unknown(void)
{
	return UNKNOWN == health_state;
}

uint8_t _task_sdcard_is_health_healthy(void)
{
	return HEALTHY == health_state;
}

uint8_t _task_sdcard_is_health_degraded(void)
{
	return DEGRADED == health_state;
}

void _task_sdcard_reset_health(void)
{
	health_state = UNKNOWN;
	sys_health.sdcard.failure_state = SDCARD_HEALTH_RESET;
	sys_health.is_updated_mask |= SYSTEM_HEALTH_UPD_SDCARD;
}

uint8_t _task_sdcard_is_formatting(void)
{
	const uint8_t ts = task_state;
	return ts >= FORMATTING_ENTRY && ts <= FORMATTING_DOEXIT;
}

void _task_sdcard_format(void)
{
	format_req_flag = 1;
}

uint8_t _task_sdcard_is_file_active(void)
{
	const uint8_t ts = task_state;
	return ts >= FILE_OPEN && ts <= FILE_CLOSE;
}

void _task_sdcard_handler(void)
{
	const uint8_t ts = task_state;
	if (ts >= POWERUP_RESET_SDCARD_CONTROLLER_ENTRY &&
	    ts <= POWERUP_SDCARD_INIT_CMD) {
		handler_powerup(ts);
	} else if (ts == READY) {
		handler_ready();
	} else if (ts >= FILE_OPEN && ts <= FILE_CLOSE) {
		handler_file(ts);
	} else if (ts == FORMATTING_DOEXIT) {
		handler_format();
	} else if (ts >= SELFTEST_ENTRY && ts <= SELFTEST_TESTMEDIA) {
		handler_selftest(ts);
	} else if (ts == FORMATTING_ENTRY) {
		sdcard_format_initsm();
		task_state = FORMATTING_DOEXIT;
	} else if (ts == STOPPED) {
		stop_req_flag = 0;

		if (format_req_flag && DEGRADED != health_state) {
			format_req_flag = 0;
			task_state = FORMATTING_ENTRY;
		} else if (selftest_req_flag) {
			selftest_req_flag = 0;
			// TODO allow when degraded or not?
			// if (DEGRADED != health_state)
			task_state = SELFTEST_ENTRY;
		}
	} else {
		// XXX this should not happen!
		_task_sdcard_init_por();
	}
}

static inline void handler_powerup(const uint8_t ts)
{
	if (ts == POWERUP_SDCARD_INIT_CMD) {
		// TODO timeout? <= 300ms
		// TODO update syshealth timout flag?
		const uint8_t diag = sdcard_init();
		if (diag == SDCARD_DIAG_COMPLETED_SUCCESS) {
			sys_health.sdcard.failure_state = SDCARD_NOFAIL;
			task_state = STOPPED;
		} else if (diag == SDCARD_DIAG_COMPLETED_FAIL) {
			sys_health.sdcard.failure_state = SDCARD_INIT_FAIL;
			sys_health.is_updated_mask |= SYSTEM_HEALTH_UPD_SDCARD;
			health_state = DEGRADED;
			task_state = STOPPED;
		}
	} else if (ts == POWERUP_RESET_SDCARD_CONTROLLER_ENTRY) {
		// reset the SD Card controller (ALFAT)
		SDCARD_PORT.OUTCLR = SDCARD_RESET_bm;
		TIMEOUT_TIMER.PER = TIMER_7US_PERIOD;
		TIMEOUT_TIMER.CTRLA = TIMER_7US_CLKSEL;
		timeout_timer_restart();
		task_state = POWERUP_RESET_SDCARD_CONTROLLER_DOEXIT;
	} else if (ts == POWERUP_RESET_SDCARD_CONTROLLER_DOEXIT) {
		if (timeout_timer_isfinished()) {
			SDCARD_PORT.OUTSET = SDCARD_RESET_bm;
			task_state = POWERUP_WAIT_SDCARD_CONTROLLER_INIT_ENTRY;
		}
	} else if (ts == POWERUP_WAIT_SDCARD_CONTROLLER_INIT_ENTRY) {
		// wait before initializing the SD Card
		TIMEOUT_TIMER.PER = TIMER_50MS_PERIOD;
		TIMEOUT_TIMER.CTRLA = TIMER_50MS_CLKSEL;
		timeout_timer_restart();
		task_state = POWERUP_WAIT_SDCARD_CONTROLLER_INIT_DOEXIT;
	} else if (ts == POWERUP_WAIT_SDCARD_CONTROLLER_INIT_DOEXIT) {
		if (timeout_timer_isfinished()) {
			sdcard_init_initsm();
			task_state = POWERUP_SDCARD_INIT_CMD;
		}
	}
}

static inline void handler_ready(void)
{
	if (stop_req_flag | selftest_req_flag | format_req_flag) {
		stop_req_flag = 0;
		task_state = STOPPED;
	} else {
		sdcard_open_initsm(get_packed_file_name());
		sdcard_fastwrite_initsm(sdcard_buffer,
					&sdcard_write_chunk,
					&sdcard_read_chunk);
		sdcard_close_initsm();
		task_state = FILE_OPEN;
	}
}

static inline void handler_file(const uint8_t ts)
{
	if (ts == FILE_OPEN) {
		handler_openfile();
	} else if (ts == FILE_FASTWRITE) {
		handler_writefile();
	} else if (ts == FILE_CLOSE) {
		handler_closefile();
	}
}

static inline void handler_openfile(void)
{
	// TODO timeout?
	// TODO update syshealth timout flag?
	const uint8_t diag = sdcard_open();
	if (diag == SDCARD_DIAG_COMPLETED_SUCCESS) {
		sys_health.sdcard.failure_state = SDCARD_NOFAIL;
		filename_needs_update = 1;
		if (stop_req_flag | selftest_req_flag | format_req_flag)
			task_state = FILE_CLOSE;
		else
			task_state = FILE_FASTWRITE;
	} else if (diag == SDCARD_DIAG_COMPLETED_FAIL) {
		sys_health.sdcard.failure_state = SDCARD_OPEN_FAIL;
		sys_health.is_updated_mask |= SYSTEM_HEALTH_UPD_SDCARD;
		health_state = DEGRADED;
		task_state = STOPPED;
	}
}

static inline void handler_writefile(void)
{
	// TODO timeout?
	// TODO update syshealth timout flag?
	const uint8_t diag = sdcard_fastwrite(stop_req_flag | selftest_req_flag | format_req_flag);
	if (diag == SDCARD_DIAG_COMPLETED_SUCCESS) {
		sys_health.sdcard.failure_state = SDCARD_NOFAIL;
		task_state = FILE_CLOSE;
	} else if (diag == SDCARD_DIAG_COMPLETED_FAIL) {
		sys_health.sdcard.failure_state = SDCARD_FASTWRITE_FAIL;
		sys_health.is_updated_mask |= SYSTEM_HEALTH_UPD_SDCARD;
		health_state = DEGRADED;
		task_state = STOPPED;
	}
}

static inline void handler_closefile(void)
{
	// TODO timeout?
	// TODO update syshealth timout flag?
	const uint8_t diag = sdcard_close();
	if (diag == SDCARD_DIAG_COMPLETED_SUCCESS) {
		sys_health.sdcard.failure_state = SDCARD_NOFAIL;
		task_state = READY;
	} else if (diag == SDCARD_DIAG_COMPLETED_FAIL) {
		sys_health.sdcard.failure_state = SDCARD_CLOSE_FAIL;
		sys_health.is_updated_mask |= SYSTEM_HEALTH_UPD_SDCARD;
		health_state = DEGRADED;
		task_state = STOPPED;
	}
}

static inline void handler_selftest(const uint8_t ts)
{
	if (ts == SELFTEST_READSTATUS) {
		handler_selftest_readstatus();
	} else if (ts == SELFTEST_FREESIZE) {
		handler_selftest_freesize();
	} else if (ts == SELFTEST_TESTMEDIA) {
		handler_selftest_testmedia();
	} else if (ts == SELFTEST_ENTRY) {
		uint8_t *const status_byte = &sys_health.sdcard.status_byte;
		uint64_t *const free_bytes = &sys_health.sdcard.free_bytes;
		uint32_t *const write_millis = &sys_health.sdcard.write_millis;
		uint32_t *const read_millis = &sys_health.sdcard.read_millis;
		sdcard_readstatus_initsm(status_byte);
		sdcard_freesize_initsm(free_bytes);
		sdcard_testmedia_initsm(write_millis, read_millis);
		task_state = SELFTEST_READSTATUS;
	}
}

static inline void handler_format(void)
{
	// TODO timeout?        <= 30s
	// TODO update syshealth timout flag?
	const uint8_t diag = sdcard_format();
	if (diag == SDCARD_DIAG_COMPLETED_SUCCESS) {
		sys_health.sdcard.failure_state = SDCARD_NOFAIL;
		task_state = SELFTEST_ENTRY;
	} else if (diag == SDCARD_DIAG_COMPLETED_FAIL) {
		sys_health.sdcard.failure_state = SDCARD_FORMAT_FAIL;
		sys_health.is_updated_mask |= SYSTEM_HEALTH_UPD_SDCARD;
		health_state = DEGRADED;
		task_state = STOPPED;
	}
}

static inline void handler_selftest_readstatus()
{
	// TODO timeout?        <= 1s
	// TODO update syshealth timout flag?
	const uint8_t diag = sdcard_readstatus();
	if (diag == SDCARD_DIAG_COMPLETED_SUCCESS) {
		sys_health.sdcard.failure_state = SDCARD_NOFAIL;
		health_state = HEALTHY;
		task_state = SELFTEST_FREESIZE;
	} else if (diag == SDCARD_DIAG_COMPLETED_FAIL) {
		sys_health.sdcard.failure_state = SDCARD_READSTATUS_FAIL;
		// TODO update syshealth timout flag?
		// if (was_timeout)
		//      sys_health.flags |= SELFTEST_SDCARD_READSTATUS_TIMEOUT;
		// else
		//      sys_health.flags &= ~SELFTEST_SDCARD_READSTATUS_TIMEOUT;
		sys_health.is_updated_mask |= SYSTEM_HEALTH_UPD_SDCARD;
		health_state = DEGRADED;
		task_state = STOPPED;
	}
}

static inline void handler_selftest_freesize()
{
	// TODO timeout?        <= 1s
	// TODO update syshealth timout flag?
	const uint8_t diag = sdcard_freesize();
	if (diag == SDCARD_DIAG_COMPLETED_SUCCESS) {
		sys_health.sdcard.failure_state = SDCARD_NOFAIL;
		health_state = HEALTHY;
		task_state = SELFTEST_TESTMEDIA;
	} else if (diag == SDCARD_DIAG_COMPLETED_FAIL) {
		sys_health.sdcard.failure_state = SDCARD_FREESIZE_FAIL;
		// TODO update syshealth timout flag?
		// if (was_timeout)
		//      sys_health.flags |= SELFTEST_SDCARD_FREESIZE_TIMEOUT;
		// else
		//      sys_health.flags &= ~SELFTEST_SDCARD_FREESIZE_TIMEOUT;
		sys_health.is_updated_mask |= SYSTEM_HEALTH_UPD_SDCARD;
		health_state = DEGRADED;
		task_state = STOPPED;
	}
}

static inline void handler_selftest_testmedia()
{
	// TODO timeout?        <= 10s
	// TODO update syshealth timout flag?
	const uint8_t diag = sdcard_testmedia();
	if (diag == SDCARD_DIAG_COMPLETED_SUCCESS) {
		sys_health.sdcard.failure_state = SDCARD_NOFAIL;
		// this is the last selftest step, so signal update even
		// when NOFAIL.
		sys_health.is_updated_mask |= SYSTEM_HEALTH_UPD_SDCARD;
		health_state = HEALTHY;
		task_state = STOPPED;
	} else if (diag == SDCARD_DIAG_COMPLETED_FAIL) {
		sys_health.sdcard.failure_state = SDCARD_TESTMEDIA_FAIL;
		// TODO update syshealth timout flag?
		// if (was_timeout)
		//      sys_health.flags |= SELFTEST_SDCARD_TESTMEDIA_TIMEOUT;
		// else
		//      sys_health.flags &= ~SELFTEST_SDCARD_TESTMEDIA_TIMEOUT;
		sys_health.is_updated_mask |= SYSTEM_HEALTH_UPD_SDCARD;
		health_state = DEGRADED;
		task_state = STOPPED;
	}
}
