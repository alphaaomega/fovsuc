/*  system.h
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// TODO rename to SYSTEM_HEALTH_H
#ifndef SYSTEM_H
#define SYSTEM_H

#include <inttypes.h>
#include "sdcard.h"

struct system_health {
	enum {
		SYSTEM_HEALTH_UPD_ADXL = 1,
		SYSTEM_HEALTH_UPD_SDCARD = 2
	} is_updated_mask;

	struct {
		enum {
			ADXL_NOFAIL = 0,
			ADXL_LOWER_BOUNDARY_FAIL = 1,
			ADXL_HEALTH_RESET = 2
		} failure_state;

		int16_t x_avg_nost;
		int16_t y_avg_nost;
		int16_t z_avg_nost;
		int16_t x_avg_st;
		int16_t y_avg_st;
		int16_t z_avg_st;
		int16_t x_diff;
		int16_t y_diff;
		int16_t z_diff;
	} adxl;

	struct {
		enum {
			SDCARD_NOFAIL,
			SDCARD_INIT_FAIL,
			SDCARD_CLOSE_FAIL,
			SDCARD_FASTWRITE_FAIL,
			SDCARD_FORMAT_FAIL,
			SDCARD_FREESIZE_FAIL,
			SDCARD_OPEN_FAIL,
			SDCARD_READSTATUS_FAIL,
			SDCARD_TESTMEDIA_FAIL,
			SDCARD_HEALTH_RESET
		} failure_state;

		enum SDCARD_DIAG_COMPLETED_FAIL_REASON reason;
		enum SDCARD_CMD_RETURN_CODE retcode;

		uint8_t status_byte;
		uint64_t free_bytes;
		uint32_t write_millis;
		uint32_t read_millis;
	} sdcard;
};

extern struct system_health sys_health;

#endif				/* SYSTEM_H */
