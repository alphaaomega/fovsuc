/*  sdcard_transactions.h
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SDCARD_TRANSACTIONS_H
#define SDCARD_TRANSACTIONS_H

#include <inttypes.h>

#define RX_BUFFER_SIZE  48
extern char rx_buffer[];

enum TRANSACTION_STATE {
	RUNNING,
	FINISHED,
	/// \note This state is only available for write transactions.
	BUSY
};

enum TRANSACTION_STATE sdcard_write_transaction(uint8_t * const header_pos,
						const char *payload,
						uint8_t * const payload_length);
enum TRANSACTION_STATE sdcard_read_transaction(uint8_t * const header_pos,
					       char *const buffer,
					       const uint8_t max_length,
					       uint8_t * const actual_length);

#endif				/* SDCARD_TRANSACTIONS_H */
