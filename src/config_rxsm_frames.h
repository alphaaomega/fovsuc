/*  config_rxsm_frames.h - constants for and structure of RXSM frames
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_RXSM_FRAMES_H
#define CONFIG_RXSM_FRAMES_H

#include <inttypes.h>

static const uint8_t RXSM_FRAME_SYNC_19_12 = 0xf9;
static const uint8_t RXSM_FRAME_SYNC_11_4 = 0xaf;
static const uint8_t RXSM_FRAME_SYNC_3_0 = 0x20;

/// Telemetry frame message IDs.
enum TELEMETRY_MSGID {
	LIVE_DATA_FRAME_A_ID = 1,
	LIVE_DATA_FRAME_B_ID = 2,
	ADXL_HEALTH_FRAME_ID = 5,
	SDCARD_HEALTH_FRAME_ID = 6,
};

/// Telecommand frame message IDs.
enum TELECOMMAND_MSGID {
	SOFTWARE_RESET_FRAME_ID = 1,
	FORCE_SELFTEST_FRAME_ID = 2,
	SDCARD_FORMAT_FRAME_ID = 3
};

/// Live Data Frame A Payload.
///
/// Payload to send current measurements to ground.
/// Used in conjunction with live_payload_b.
struct live_payload_a {
	/// Fiber primary path photodiode sensor high byte.
	uint8_t fiberApri_15_8;
	/// Fiber primary path photodiode sensor low byte.
	uint8_t fiberApri_7_0;
	/// Fiber secondary path photodiode sensor high byte.
	uint8_t fiberAsec_15_8;
	/// Fiber secondary path photodiode sensor low byte.
	uint8_t fiberAsec_7_0;

	/// Fiber primary path photodiode sensor high byte.
	uint8_t fiberBpri_15_8;
	/// Fiber primary path photodiode sensor low byte.
	uint8_t fiberBpri_7_0;
	/// Fiber secondary path photodiode sensor high byte.
	uint8_t fiberBsec_15_8;
	/// Fiber secondary path photodiode sensor low byte.
	uint8_t fiberBsec_7_0;

	/// Fiber primary path photodiode sensor high byte.
	uint8_t fiberCpri_15_8;
	/// Fiber primary path photodiode sensor low byte.
	uint8_t fiberCpri_7_0;
	/// Fiber secondary path photodiode sensor high byte.
	uint8_t fiberCsec_15_8;
	/// Fiber secondary path photodiode sensor low byte.
	uint8_t fiberCsec_7_0;

	/// Fiber primary path photodiode sensor high byte.
	uint8_t fiberDpri_15_8;
	/// Fiber primary path photodiode sensor low byte.
	uint8_t fiberDpri_7_0;
	/// Fiber secondary path photodiode sensor high byte.
	uint8_t fiberDsec_15_8;
	/// Fiber secondary path photodiode sensor low byte.
	uint8_t fiberDsec_7_0;

	/// Part of timestamp counter.
	uint8_t counter_23_16;
	/// Part of timestamp counter.
	uint8_t counter_15_8;
	/// Part of timestamp counter.
	uint8_t counter_7_0;
};

/// Live Data Frame B Payload.
///
/// Payload to send current measurements to ground.
/// Used in conjunction with live_payload_a.
struct live_payload_b {
	/// Fiber temperature sensor high byte.
	uint8_t fiberAtemp_15_8;
	/// Fiber temperature sensor low byte.
	uint8_t fiberAtemp_7_0;
	/// Fiber temperature sensor high byte.
	uint8_t fiberBtemp_15_8;
	/// Fiber temperature sensor low byte.
	uint8_t fiberBtemp_7_0;
	/// Fiber temperature sensor high byte.
	uint8_t fiberCtemp_15_8;
	/// Fiber temperature sensor low byte.
	uint8_t fiberCtemp_7_0;
	/// Fiber temperature sensor high byte.
	uint8_t fiberDtemp_15_8;
	/// Fiber temperature sensor low byte.
	uint8_t fiberDtemp_7_0;

	/// Low resolution temperature sensor high byte.
	uint8_t lm74adxl_15_8;
	/// Low resolution temperature sensor low byte.
	uint8_t lm74adxl_7_0;

	/// Low resolution temperature sensor high byte.
	uint8_t lm74local_15_8;
	/// Low resolution temperature sensor low byte.
	uint8_t lm74local_7_0;

	/// Acceleration in X dimension high byte.
	uint8_t dimx_15_8;
	/// Acceleration in X dimension low byte.
	uint8_t dimx_7_0;

	/// Acceleration in Y dimension high byte.
	uint8_t dimy_15_8;
	/// Acceleration in Y dimension low byte.
	uint8_t dimy_7_0;

	/// Acceleration in Z dimension high byte.
	uint8_t dimz_15_8;
	/// Acceleration in Z dimension low byte.
	uint8_t dimz_7_0;

	/// High resolution auxiliary sensors.
	uint8_t res_2_0__soe_0__lo_0__sods_0__loo_0__pta_0;
};

/// ADXL Health Frame payload.
///
/// Payload to send results of ADXL selftest to ground.
struct adxl_health_payload {
	uint8_t failure_state;

	uint8_t x_avg_nost_15_8;
	uint8_t x_avg_nost_7_0;
	uint8_t y_avg_nost_15_8;
	uint8_t y_avg_nost_7_0;
	uint8_t z_avg_nost_15_8;
	uint8_t z_avg_nost_7_0;

	uint8_t x_avg_st_15_8;
	uint8_t x_avg_st_7_0;
	uint8_t y_avg_st_15_8;
	uint8_t y_avg_st_7_0;
	uint8_t z_avg_st_15_8;
	uint8_t z_avg_st_7_0;

	uint8_t x_diff_15_8;
	uint8_t x_diff_7_0;
	uint8_t y_diff_15_8;
	uint8_t y_diff_7_0;
	uint8_t z_diff_15_8;
	uint8_t z_diff_7_0;
};

/// SD Card Health Frame payload.
///
/// Payload to send results of SD Card operations and selftest to ground.
struct sdcard_health_payload {
	uint8_t failure_state_3_0__reason_3_0;
	uint8_t retcode;

	uint8_t status_byte;

	uint8_t free_bytes_63_56;
	uint8_t free_bytes_55_48;
	uint8_t free_bytes_47_40;
	uint8_t free_bytes_39_32;
	uint8_t free_bytes_31_24;
	uint8_t free_bytes_23_16;
	uint8_t free_bytes_15_8;
	uint8_t free_bytes_7_0;

	uint8_t write_millis_31_24;
	uint8_t write_millis_23_16;
	uint8_t write_millis_15_8;
	uint8_t write_millis_7_0;

	uint8_t read_millis_31_24;
	uint8_t read_millis_23_16;
	uint8_t read_millis_15_8;
	uint8_t read_millis_7_0;
};

/// A generic frame transmitted via RXSM Up- and Downlink interface.
union rxsm_frame {
	/// Element-wise access to frame contents with variable payload data.
	struct {
		/// The sync sequence high byte.
		uint8_t sync_19_12;
		/// Sync sequence middle byte.
		uint8_t sync_11_4;
		/// Sync sequence low byte and message ID.
		uint8_t sync_3_0__msgid_3_0;
		union {
			/// Live Data Payload Type A.
			struct live_payload_a la;
			/// Live Data Payload Type B.
			struct live_payload_b lb;
			/// ADXL Health Payload.
			struct adxl_health_payload ap;
			/// SD Card Health Payload.
			struct sdcard_health_payload sd;
		} p;		///< The polymorph payload data.
		/// CRC-CCITT-16 high byte.
		uint8_t crc_15_8;
		/// CRC-CCITT-16 low byte.
		uint8_t crc_7_0;
	} f;			///< Instance of a generic frame with variable payload.
	/// Byte-wise access.
	uint8_t byte[24];
};

#endif				/* CONFIG_RXSM_FRAMES_H */
