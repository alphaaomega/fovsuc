/*  task_lores_temperature.c - task to read LM74 temperature sensors and
 *                             uC-internal ADC measurements
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tasking.h"

static inline void handler_lm74(void);
static inline void handler_adcs(const uint8_t ts);

static enum {
	READY,
	ADC_25REF_START,
	ADC_25REF_STOP__ADC25PWR_START,
	ADC_25PWR_STOP__TEMPSENSE_START,
	ADC_TEMPSENSE_STOP,
	READ_LM74,
	STOPPED
} task_state;

static uint8_t invocation_count;
static uint8_t stop_req_flag;

void _task_lores_temperature_init_por(void)
{
	invocation_count = 0;
	stop_req_flag = 0;
	task_state = READY;
}

uint8_t _task_lores_temperature_is_stopped(void)
{
	return STOPPED == task_state;
}

void _task_lores_temperature_stop(void)
{
	if (STOPPED != task_state)
		stop_req_flag = 1;
}

void _task_lores_temperature_resume(void)
{
	if (STOPPED == task_state)
		task_state = READY;
}

void _task_lores_temperature_handler(void)
{
	const uint8_t ts = task_state;
	if (ts == READ_LM74) {
		handler_lm74();
		task_state = READY;
	} else if (ts >= ADC_25REF_START && ts <= ADC_TEMPSENSE_STOP) {
		handler_adcs(ts);
	} else if (ts == READY) {
		if (stop_req_flag) {
			task_state = STOPPED;
		} else {
			// run only every 48th invocation,
			// one complete run takes 5 invocations
			++invocation_count;
			if (invocation_count < 48)
				return;
			invocation_count = 0;
			task_state = ADC_25REF_START;
		}
	} else if (ts == STOPPED) {
		invocation_count = 0;
		stop_req_flag = 0;
	} else {
		// XXX should never happen!
		_task_lores_temperature_init_por();
	}
}

static inline void handler_lm74(void)
{
	// shutdown UART/MSPI, load settings for LM74, enable UART/MSPI
	AUXBUS_UART_MSPI.CTRLB &= ~(USART_RXEN_bm | USART_TXEN_bm);
	AUXBUS_PORT.DIRSET = AUXBUS_MOSI_bm;
	AUXBUS_PORT.AUXBUS_SCK_PINCTRL = AUXBUS_LM74_SCK_PINCTRL;
	AUXBUS_UART_MSPI.CTRLC = USART_CMODE_MSPI_gc | AUXBUS_LM74_UCPHA;
	AUXBUS_UART_MSPI.CTRLB = USART_RXEN_bm | USART_TXEN_bm;

	//----------------------------------------------------------------------
	// (1)  activate local LM74 sensor
	//      and restart the clock generator
	LM74LOCAL_CS_PORT.OUTCLR = LM74LOCAL_CS_bm;
	AUXBUS_UART_MSPI.BAUDCTRLA = 0;
	AUXBUS_UART_MSPI.BAUDCTRLB = 0;
	AUXBUS_UART_MSPI.BAUDCTRLA = AUXBUS_LM74_BAUDCTRLA;
	AUXBUS_UART_MSPI.BAUDCTRLB = AUXBUS_LM74_BAUDCTRLB;
	// (2)  enqueue bytes 1..2/1..2 for local LM74 sensor
	AUXBUS_UART_MSPI.DATA = 0;
	do {
		asm volatile ("");
	} while (!(AUXBUS_UART_MSPI.STATUS & USART_DREIF_bm));
	AUXBUS_UART_MSPI.DATA = 0;

	// retrieve buffer destination for this session
	// and update pointer for next usage
	const uint8_t buffer_safe = sdcard_buffer_overrun_safe;
	const uint16_t chunk_pos = sdcard_write_chunk_position;
	if (buffer_safe) {
		sdcard_write_chunk_position = chunk_pos
		    + sizeof(union lores_temperature);
	}
	uint8_t *const sdcard_buf_ptr = sdcard_buffer
	    + sdcard_write_chunk * SDCARD_CHUNK_SIZE + chunk_pos;

	// initialize CRC module with "0" pattern (all zeros)
	CRC.CTRL = CRC_RESET_RESET0_gc;
	CRC.CTRL = CRC_SOURCE_IO_gc;

	do {
		asm volatile ("");
	} while (!(AUXBUS_UART_MSPI.STATUS & USART_RXCIF_bm));
	const uint8_t lm74local_12_5 = AUXBUS_UART_MSPI.DATA;

	union lores_temperature *const lo = &lores_temperature_buffer;
	lo->e.id__res_4_0 = (uint8_t) (LORES_TEMPERATURE_RECORD_ID << 5);
	lo->e.res_2_0__lm74local_12_8 = lm74local_12_5 >> 3;
	lo->e.lm74local_7_0 = (uint8_t) (lm74local_12_5 << 5);

	// wait for recv byte 2/1..2 from local LM74 sensor
	do {
		asm volatile ("");
	} while (!(AUXBUS_UART_MSPI.STATUS & USART_RXCIF_bm));
	const uint8_t lm74local_4_0 = AUXBUS_UART_MSPI.DATA >> 3;
	lo->e.lm74local_7_0 |= lm74local_4_0;

	// (4)  disable local LM74 sensor
	LM74LOCAL_CS_PORT.OUTSET = LM74LOCAL_CS_bm;
	//----------------------------------------------------------------------

	//----------------------------------------------------------------------
	// (1)  activate LM74 sensor on ADXL PCB (external)
	//      and restart the clock generator
	LM74ADXL_CS_PORT.OUTCLR = LM74ADXL_CS_bm;
	AUXBUS_UART_MSPI.BAUDCTRLA = 0;
	AUXBUS_UART_MSPI.BAUDCTRLB = 0;
	AUXBUS_UART_MSPI.BAUDCTRLA = AUXBUS_LM74_BAUDCTRLA;
	AUXBUS_UART_MSPI.BAUDCTRLB = AUXBUS_LM74_BAUDCTRLB;
	// (2)  enqueue bytes 1..2/1..2 for external LM74 sensor
	AUXBUS_UART_MSPI.DATA = 0;
	do {
		asm volatile ("");
	} while (!(AUXBUS_UART_MSPI.STATUS & USART_DREIF_bm));
	AUXBUS_UART_MSPI.DATA = 0;

	do {
		asm volatile ("");
	} while (!(AUXBUS_UART_MSPI.STATUS & USART_RXCIF_bm));
	const uint8_t lm74adxl_12_5 = AUXBUS_UART_MSPI.DATA;

	lo->e.res_2_0__lm74adxl_12_8 = (uint8_t) (lm74adxl_12_5 >> 3);
	lo->e.lm74adxl_7_0 = (uint8_t) (lm74adxl_12_5 << 5);

	do {
		asm volatile ("");
	} while (!(AUXBUS_UART_MSPI.STATUS & USART_RXCIF_bm));
	const uint8_t lm74adxl_4_0 = AUXBUS_UART_MSPI.DATA >> 3;

	lo->e.lm74adxl_7_0 |= lm74adxl_4_0;

	// (4)  disable external (ADXL) LM74 sensor
	LM74ADXL_CS_PORT.OUTSET = LM74ADXL_CS_bm;
	//----------------------------------------------------------------------

	// only if sdcard buffer clean
	if (buffer_safe) {
		for (uint8_t i = 0;
		     i < sizeof(lores_temperature_buffer) - 2; ++i) {
			const uint8_t byte = lores_temperature_buffer.byte[i];
			CRC.DATAIN = byte;
			sdcard_buf_ptr[i] = byte;
		}
	} else {
		for (uint8_t i = 0;
		     i < sizeof(lores_temperature_buffer) - 2; ++i) {
			const uint8_t byte = lores_temperature_buffer.byte[i];
			CRC.DATAIN = byte;
		}
	}

	lo->e.crc_7_0 = CRC.CHECKSUM0;
	lo->e.crc_15_8 = CRC.CHECKSUM1;
	if (buffer_safe) {
		sdcard_buf_ptr[11] = lores_temperature_buffer.byte[11];
		sdcard_buf_ptr[12] = lores_temperature_buffer.byte[12];
		sdcard_buffer_overrun_safe = 0;
	}
	// restore ADXL settings but reset the clock generator
	AUXBUS_UART_MSPI.CTRLB &= ~(USART_RXEN_bm | USART_TXEN_bm);
	AUXBUS_PORT.DIRSET = AUXBUS_MOSI_bm;
	AUXBUS_PORT.AUXBUS_SCK_PINCTRL = AUXBUS_ADXL312_SCK_PINCTRL;
	AUXBUS_UART_MSPI.CTRLC = USART_CMODE_MSPI_gc | AUXBUS_ADXL312_UCPHA;
	AUXBUS_UART_MSPI.BAUDCTRLA = 0;
	AUXBUS_UART_MSPI.BAUDCTRLB = 0;
	AUXBUS_UART_MSPI.CTRLB = USART_RXEN_bm | USART_TXEN_bm;
}

static inline void handler_adcs(const uint8_t ts)
{
	if (ts == ADC_25REF_START) {
		ADCA.CTRLB |= ADC_CONMODE_bm;
		ADCA.REFCTRL = ADC_REFSEL_VCC_gc | ADC_BANDGAP_bm | ADC_TEMPREF_bm;	// VCC/1.6
		ADCA.INTFLAGS |= ADC_CH0IF_bm;
		ADCA.CH0.CTRL = ADC_CH_GAIN_DIV2_gc
		    | ADC_CH_INPUTMODE_DIFFWGAIN_gc;
		ADCA.CH0.MUXCTRL =
		    ADC_CH_MUXPOS_25REF | ADC_CH_MUXNEG_PADGND_WGAIN;
		ADCA.CH0.INTCTRL = 0;
		ADCA.CTRLA |= ADC_FLUSH_bm;
		ADCA.CTRLA |= ADC_CH0START_bm;
		task_state = ADC_25REF_STOP__ADC25PWR_START;
	} else if (ts == ADC_25REF_STOP__ADC25PWR_START) {
		const uint16_t val = ADCA.CH0.RES;
		union lores_temperature *const lo = &lores_temperature_buffer;
		lo->e.res_3_0__adc25ref_11_8 = (val >> 8) & 0x0f;
		lo->e.adc25ref_7_0 = val & 0xff;

		ADCA.CTRLB |= ADC_CONMODE_bm;
		ADCA.REFCTRL = ADC_REFSEL_VCC_gc | ADC_BANDGAP_bm | ADC_TEMPREF_bm;	// VCC/1.6
		ADCA.INTFLAGS |= ADC_CH0IF_bm;
		ADCA.CH0.CTRL = ADC_CH_GAIN_DIV2_gc
		    | ADC_CH_INPUTMODE_DIFFWGAIN_gc;
		ADCA.CH0.MUXCTRL =
		    ADC_CH_MUXPOS_25PWR | ADC_CH_MUXNEG_PADGND_WGAIN;
		ADCA.CH0.INTCTRL = 0;
		ADCA.CTRLA |= ADC_FLUSH_bm;
		ADCA.CTRLA |= ADC_CH0START_bm;
		task_state = ADC_25PWR_STOP__TEMPSENSE_START;
	} else if (ts == ADC_25PWR_STOP__TEMPSENSE_START) {
		const uint16_t val = ADCA.CH0.RES;
		union lores_temperature *const lo = &lores_temperature_buffer;
		lo->e.res_3_0__adc25pwr_11_8 = (val >> 8) & 0x0f;
		lo->e.adc25pwr_7_0 = val & 0xff;

		ADCA.CTRLB &= ~ADC_CONMODE_bm;
		ADCA.REFCTRL = ADC_REFSEL_INT1V_gc | ADC_BANDGAP_bm | ADC_TEMPREF_bm;	// 1V Bandgap
		ADCA.INTFLAGS |= ADC_CH0IF_bm;
		ADCA.CH0.CTRL = ADC_CH_INPUTMODE_INTERNAL_gc | ADC_CH_GAIN_1X_gc;
		ADCA.CH0.MUXCTRL = ADC_CH_MUXINT_TEMP_gc;
		ADCA.CH0.INTCTRL = 0;
		ADCA.CTRLA |= ADC_FLUSH_bm;
		ADCA.CTRLA |= ADC_CH0START_bm;
		task_state = ADC_TEMPSENSE_STOP;
	} else if (ts == ADC_TEMPSENSE_STOP) {
		const uint16_t val = ADCA.CH0.RES;
		union lores_temperature *const lo = &lores_temperature_buffer;
		lo->e.res_3_0__uCtemp_11_8 = (val >> 8) & 0x0f;
		lo->e.uCtemp_7_0 = val & 0xff;
		task_state = READ_LM74;
	}
}
