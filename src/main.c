/*  main.c - main for fovsuc
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/interrupt.h>
#include "init.h"
#include "tasking.h"
#include "filename.h"
#include "sched.h"

int main(void)
{
	init();
	init_shared_variables();
	search_next_free_filename();

	tasking_init();
	sched_init();		// XXX move to init()
	sched_start();
	sei();

	while (1) {
		asm volatile ("");
	}
	return 0;
}
