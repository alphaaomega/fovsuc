/*  sdcard_readstatus.c - command to retrieve status from SD Card module
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sdcard.h"
#include "sdcard_transactions.h"
#include "sdcard_helper.h"

// TODO move to config_alfat.h?
/// Bit mask for ALFAT READSTATUS byte CARDDETECTED bit.
#define SDCARD_STATUS_CARDDETECTED_bm   _BV(0)
/// Bit mask for ALFAT READSTATUS byte CARDPROTECTED bit.
#define SDCARD_STATUS_CARDPROTECTED_bm  _BV(1)

/// The state of the internal state machine.
static enum READSTATUS_STATE {
	CLEAR_FIFO,
	CMD,
	RET,
	CHECK_RET,
	STATUSRET,
	CHECK_STATUSRET
} state;

static void cmd_callback(void);
static void busy_callback(void);
static void check_ret_success_callback(void);
static enum SDCARD_DIAG_BASE state_readstatus_checkstatusret(void);

/// ALFAT command to read the status byte te SD Card.
static const char ALFAT_READSTATUS_CMD[] = "J\n";

/// The reference to the location to save the status byte to.
static uint8_t *status_byte;

static uint8_t header_pos;
static uint8_t total_received_bytes;

void sdcard_readstatus_initsm(uint8_t * const status)
{
	state = CLEAR_FIFO;
	header_pos = 0;
	total_received_bytes = 0;
	status_byte = status;
}

/// Send read_status "J" command to SD Card.
enum SDCARD_DIAG_BASE sdcard_readstatus(void)
{
	if (state == CLEAR_FIFO)
		return sdcard_helper_clear_fifo(&header_pos, &state, CMD);
	if (state == CMD)
		return sdcard_helper_cmd(&header_pos, ALFAT_READSTATUS_CMD,
					 sizeof(ALFAT_READSTATUS_CMD) - 1,
					 &busy_callback, &cmd_callback);
	if (state == RET)
		return sdcard_helper_receive(&header_pos, &state,
					     &total_received_bytes, 4,
					     CHECK_RET);
	if (state == STATUSRET)
		return sdcard_helper_receive(&header_pos, &state,
					     &total_received_bytes, 8,
					     CHECK_STATUSRET);
	if (state == CHECK_RET)
		return sdcard_helper_check_cmderrno(0,
						    &check_ret_success_callback,
						    SDCARD_DIAG_PROGRESS);
	return state_readstatus_checkstatusret();
}

static void cmd_callback(void)
{
	total_received_bytes = 0;
	state = RET;
}

static void busy_callback(void)
{
	state = CLEAR_FIFO;
	header_pos = 0;
	total_received_bytes = 0;
}

static void check_ret_success_callback(void)
{
	header_pos = 0;
	total_received_bytes = 0;
	state = STATUSRET;
}

static enum SDCARD_DIAG_BASE state_readstatus_checkstatusret(void)
{
	const char *const buffer = rx_buffer;
	if (!(buffer[4] == '!' && buffer[7] == '\n')) {
		sys_health.sdcard.reason = REASON_RETCODE_DELIM;
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	const uint8_t retcode = hex2int(buffer[5]) << 4 | hex2int(buffer[6]);
	if (retcode != SDCARD_CMD_RET_SUCCESS) {
		sys_health.sdcard.reason = REASON_RETCODE_VALUE;
		sys_health.sdcard.retcode = retcode;
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	if (!(buffer[0] == '$' && buffer[3] == '\n')) {
		sys_health.sdcard.reason = REASON_DATA_DELIM;
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	*status_byte = hex2int(buffer[1]) << 4 | hex2int(buffer[2]);
	const uint8_t mask =
	    SDCARD_STATUS_CARDDETECTED_bm | SDCARD_STATUS_CARDPROTECTED_bm;
	if ((*status_byte & mask) == SDCARD_STATUS_CARDDETECTED_bm) {
		return SDCARD_DIAG_COMPLETED_SUCCESS;
	}

	sys_health.sdcard.reason = REASON_READSTATUS_FLAGERROR;
	return SDCARD_DIAG_COMPLETED_FAIL;
}
