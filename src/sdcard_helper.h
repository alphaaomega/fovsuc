/*  sdcard_helper.h - inlined utility functions for SD Card interfacing
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SDCARD_HELPER_H
#define SDCARD_HELPER_H

#include "sdcard.h"
#include "config.h"		/* SDCARD_PORT, SDCARD_ACTIVE_bm */
#include "system.h"		/* TODO not nice, but needed for sys_health */

// TODO maybe rename to sdcard_base.. or sdcard_generic...

static inline uint8_t hex2int(char c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	c &= ~_BV(5);		// translate char to lower case
	if (c >= 'a' && c <= 'f')
		return c - 'a';
	return 0;
}

static inline enum SDCARD_DIAG_BASE
sdcard_helper_clear_fifo(uint8_t * const header_pos, uint8_t * const state,
			 const uint8_t next_cmd)
{
	static uint8_t actual_size;
	if (*header_pos == 0) {
		if (!(SDCARD_PORT.IN & SDCARD_ACTIVE_bm)) {
			*header_pos = 0;
			*state = next_cmd;
			return SDCARD_DIAG_PROGRESS;
		}
		actual_size = 0;
	}

	enum TRANSACTION_STATE s =
	    sdcard_read_transaction(header_pos, rx_buffer, RX_BUFFER_SIZE,
				    &actual_size);
	if (s == FINISHED) {
		*header_pos = 0;
		if (!(SDCARD_PORT.IN & SDCARD_ACTIVE_bm)) {
			*state = next_cmd;
		}
	}
	return SDCARD_DIAG_PROGRESS;
}

static inline enum SDCARD_DIAG_BASE
sdcard_helper_receive(uint8_t * const header_pos, uint8_t * const state,
		      uint8_t * const already_received,
		      const uint8_t data_length, const uint8_t next_state)
{
	static uint8_t actual_size;
	if (*header_pos == 0)
		actual_size = 0;

	char *const buf = rx_buffer + *already_received;
	const uint8_t remaining_bytes = data_length - *already_received;
	enum TRANSACTION_STATE s =
	    sdcard_read_transaction(header_pos, buf, remaining_bytes,
				    &actual_size);
	if (s == FINISHED) {
		*header_pos = 0;
		*already_received += actual_size;
		if (data_length == *already_received) {
			*state = next_state;
			return SDCARD_DIAG_PROGRESS;
		}
		return SDCARD_DIAG_PARTIAL;
	}
	return SDCARD_DIAG_PROGRESS;
}

static inline enum SDCARD_DIAG_BASE
sdcard_helper_cmd(uint8_t * const header_pos, const char *const command,
		  const uint8_t length, void (*const busy_callback) (void),
		  void (*const success_callback) (void))
{
	static uint8_t missing_bytes;
	if (*header_pos == 0)
		missing_bytes = length;

	enum TRANSACTION_STATE s =
	    sdcard_write_transaction(header_pos, command, &missing_bytes);
	if (s == BUSY) {
		if (busy_callback != 0)
			busy_callback();
		return SDCARD_DIAG_BUSY;
	}
	if (s == FINISHED) {
		*header_pos = 0;
		if (success_callback != 0)
			success_callback();
	}
	return SDCARD_DIAG_PROGRESS;
}

/// Checks the error code of an SD Card command.
/// The replies to a command have pattern "!XX\n" with XX being the error
/// code in hex.
///
/// \param fail_callback a function that is called on error condition
///     prior to exiting this function.
/// \param success_callback a function that is called on success prior
//      to exiting this function.
/// \param ret_success the value to return upon success.
/// \return one of ret_success, SDCARD_DIAG_COMPLETED_FAIL.
static inline enum SDCARD_DIAG_BASE
sdcard_helper_check_cmderrno(void (*const fail_callback) (void),
			     void (*const success_callback) (void),
			     const enum SDCARD_DIAG_BASE ret_success)
{
	const char *const buffer = rx_buffer;
	if (!(buffer[0] == '!' && buffer[3] == '\n')) {
		sys_health.sdcard.reason = REASON_RETCODE_DELIM;
		if (fail_callback != 0)
			fail_callback();
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	const uint8_t retcode = hex2int(buffer[1]) << 4 | hex2int(buffer[2]);
	if (retcode != SDCARD_CMD_RET_SUCCESS) {
		sys_health.sdcard.reason = REASON_RETCODE_VALUE;
		sys_health.sdcard.retcode = retcode;
		if (fail_callback != 0)
			fail_callback();
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	if (success_callback != 0)
		success_callback();
	return ret_success;
}

#endif				/* SDCARD_HELPER_H */
