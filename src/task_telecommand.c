/*  task_telecommand.c - task to receive commands through the umbilical
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tasking.h"
#include "config_rxsm_frames.h"
#include "common.h"

static inline void handler_receive_frame(void);

static enum {
	RUNNING,
	STOPPED
} task_state;

static uint8_t current_byte;

void _task_telecommand_init_por(void)
{
	current_byte = 0;
	task_state = RUNNING;
}

uint8_t _task_telecommand_is_stopped(void)
{
	return STOPPED == task_state;
}

void _task_telecommand_stop(void)
{
	task_state = STOPPED;
}

void _task_telecommand_resume(void)
{
	task_state = RUNNING;
}

void _task_telecommand_handler(void)
{
	const uint8_t ts = task_state;
	if (ts == RUNNING) {
		handler_receive_frame();
	} else if (ts == STOPPED) {
		// do nothing
		const uint8_t ignore0 __attribute__ ((unused)) = RXSM_UART.DATA;
		const uint8_t ignore1 __attribute__ ((unused)) = RXSM_UART.DATA;
		current_byte = 0;
	}
}

static inline void handler_receive_frame(void)
{
	// XXX refactor static initializer?
	static union rxsm_frame buffer = { {0} };

	// on erroneous frame reception, clear buffer
	const uint8_t error_flags = USART_FERR_bm | USART_BUFOVF_bm
	    | USART_PERR_bm;
	if (RXSM_UART.STATUS & error_flags) {
		// force clear of flags
		const uint8_t ignore0 __attribute__ ((unused)) = RXSM_UART.DATA;
		const uint8_t ignore1 __attribute__ ((unused)) = RXSM_UART.DATA;
		RXSM_UART.STATUS &= ~error_flags;
		current_byte = 0;
		return;
	}
	// quit, if there's no new data to process
	if (!(RXSM_UART.STATUS & USART_RXCIF_bm))
		return;

	const uint8_t data = RXSM_UART.DATA;
	++current_byte;

	// add new byte at the end of FIFO buffer
	for (uint8_t i = 0; i < sizeof(buffer) - 1; ++i)
		buffer.byte[i] = buffer.byte[i + 1];
	buffer.byte[sizeof(buffer) - 1] = data;

	// quit, if not a complete frame received
	if (current_byte < sizeof(buffer))
		return;
	// reset counter to max value, if frame is complete
	if (current_byte > sizeof(buffer))
		current_byte = sizeof(buffer);

	// quit, if no sync sequence found
	if (!(buffer.f.sync_19_12 == RXSM_FRAME_SYNC_19_12
	      && buffer.f.sync_11_4 == RXSM_FRAME_SYNC_11_4
	      && (buffer.f.sync_3_0__msgid_3_0 & 0xf0) == RXSM_FRAME_SYNC_3_0))
		return;

	// initialize CRC module with "0" pattern (all zeros) and calculate CRC
	CRC.CTRL = CRC_RESET_RESET0_gc;
	CRC.CTRL = CRC_SOURCE_IO_gc;
	for (uint8_t i = 0; i < sizeof(buffer); ++i)
		CRC.DATAIN = buffer.byte[i];

	// read CRC low byte first (order matters: "accessing 16-bit registers")
	const uint8_t crc_7_0 = CRC.CHECKSUM0;
	const uint8_t crc_15_8 = CRC.CHECKSUM1;
	// quit, if CRC mismatch
	if (!(crc_7_0 == 0 && crc_15_8 == 0))
		return;

	// do nothing if /LO or /SODS
	const uint8_t lo_active = !(rxsm_signal_status & RXSM_LO_bm);
	const uint8_t sods_active = !(rxsm_signal_status & RXSM_SODS_bm);
	if (lo_active || sods_active)
		return;

	// apparently sync-sequence and CRC are valid
	const enum TELECOMMAND_MSGID msgid =
	    buffer.f.sync_3_0__msgid_3_0 & 0x0f;
	switch (msgid) {
	case SOFTWARE_RESET_FRAME_ID:
		software_reset();
		break;
	case FORCE_SELFTEST_FRAME_ID:
		pcb_task_accel_adxl.selftest();
		pcb_task_sdcard.selftest();
		break;
	case SDCARD_FORMAT_FRAME_ID:
		pcb_task_sdcard.format();
		filename_needs_erase = 1;
		break;
	default:
		// do nothing. invalid MsgID
		break;
	}
}
