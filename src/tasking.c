/*  tasking.c - initialisation of tasks and shared variables
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tasking.h"
#include "system.h"

// TODO  add documentation
// TODO remove static initializers
union fiber_acceleration fiber_acceleration_buffer;
union hires_temperature hires_temperature_buffer;
union lores_temperature lores_temperature_buffer;
union reference_acceleration reference_acceleration_buffer;

struct bigmembuffer _do_not_use __attribute__ ((section(".noinit")));

/// The currently active chunk for reads from the buffer.
uint8_t sdcard_read_chunk;
/// The currently active chunk for writes to the buffer.
uint8_t sdcard_write_chunk;
/// The index of the next free byte in the active chunk.
uint16_t sdcard_write_chunk_position;

/// The states of LO, SOE, and SODS lines from RXSM interface.
/// A value of '0' means 'active', and '1' means 'inactive'.
uint8_t rxsm_signal_status;

/// A flag that signals whether the SD Card buffer has been checked for overrun.
/// If '0' the buffer must not be written, if non-'0' writing is allowed.
uint8_t sdcard_buffer_overrun_safe;

/// A flag that indicates that the filename was updated and now shall be written
/// to EEPROM.
uint8_t filename_needs_update;

/// A flag that indicated that the EEPROM shall be erased.
uint8_t filename_needs_erase;

struct system_health sys_health;

void init_shared_variables(void)
{
	// init shared variables for all tasks
	for (uint8_t i = 0; i < sizeof(fiber_acceleration_buffer); ++i)
		fiber_acceleration_buffer.byte[i] = 0;
	for (uint8_t i = 0; i < sizeof(hires_temperature_buffer); ++i)
		hires_temperature_buffer.byte[i] = 0;
	for (uint8_t i = 0; i < sizeof(lores_temperature_buffer); ++i)
		lores_temperature_buffer.byte[i] = 0;
	for (uint8_t i = 0; i < sizeof(reference_acceleration_buffer); ++i)
		reference_acceleration_buffer.byte[i] = 0;

	sdcard_read_chunk = 0;
	sdcard_write_chunk = 0;
	sdcard_write_chunk_position = 0;

	// take currently available signals as default
	rxsm_signal_status = RXSM_CTRL.IN
	    & (RXSM_SOE_bm | RXSM_LO_bm | RXSM_SODS_bm);

	sdcard_buffer_overrun_safe = 0;
	filename_needs_update = 0;
	filename_needs_erase = 0;

	sys_health.is_updated_mask = 0;
}

void tasking_init(void)
{
	pcb_task_loader.init_por();
	pcb_task_sample.init_por();
	pcb_task_accel_fiber.init_por();
	pcb_task_accel_adxl.init_por();
	pcb_task_telemetry.init_por();
	pcb_task_telecommand.init_por();
	pcb_task_sdcard.init_por();
	pcb_task_lores_temperature.init_por();
	pcb_task_hires_temperature.init_por();
	pcb_task_dummy.init_por();
}
