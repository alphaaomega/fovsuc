/*  task_hires_temperature.c - task to read NTC temperature measuments
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tasking.h"
#include "common.h"

static inline void handler_hires_temperature(void);
static inline void wait_transmission(void);

static enum {
	RUNNING,
	STOPPED
} task_state;

void _task_hires_temperature_init_por(void)
{
	task_state = RUNNING;
}

uint8_t _task_hires_temperature_is_stopped(void)
{
	return STOPPED == task_state;
}

void _task_hires_temperature_stop(void)
{
	task_state = STOPPED;
}

void _task_hires_temperature_resume(void)
{
	task_state = RUNNING;
}

void _task_hires_temperature_handler(void)
{
	const uint8_t ts = task_state;
	if (ts == RUNNING) {
		handler_hires_temperature();
	} else if (ts == STOPPED) {
		// do nothing
	}
}

/// Reads four fiber temperature and two auxiliary temperature sensors.
///
/// The SPI Write Collision Flag is ignored.
static inline void handler_hires_temperature(void)
{
	// trigger ADC conversion
	PDTEMP_PORT.OUTSET = PDTEMP_CNV_bm;

	const uint8_t buffer_safe = sdcard_buffer_overrun_safe;

	// retrieve buffer destination for this session
	// and update pointer for next usage
	const uint16_t chunk_pos = sdcard_write_chunk_position;
	if (buffer_safe) {
		sdcard_write_chunk_position = chunk_pos
		    + sizeof(union hires_temperature);
	}
	uint8_t *sdcard_buf_ptr = sdcard_buffer
	    + sdcard_write_chunk * SDCARD_CHUNK_SIZE + chunk_pos;
	FIX_POINTER(sdcard_buf_ptr);

	LED_CHIP_PORT.OUTCLR = LED_CHIPA_bm | LED_CHIPB_bm | LED_CHIPC_bm
	    | LED_CHIPD_bm;

#ifdef FOVS_FINAL
	// (1)  omit the fiber ADC's busy flag
	PDTEMP_PORT.OUTCLR = PDTEMP_SCK_bm;
	nop(2);
	PDTEMP_PORT.OUTSET = PDTEMP_SCK_bm;
	nop(2);
#endif

	uint8_t data_adc;

	// (1)  start recv: byte 01/1..14; Loss of Output           high byte
	PDTEMP_SPI.DATA = 0;
	// (2)  initialize CRC module with "0" pattern (all zeros)
	// (3)  set message ID
	CRC.CTRL = CRC_RESET_RESET0_gc;
	CRC.CTRL = CRC_SOURCE_IO_gc;

	hires_temperature_buffer.e.id__reserved =
	    HIRES_TEMPERATURE_RECORD_ID << 5;
	//----------------------------------------------------------------------

	wait_transmission();

	//----------------------------------------------------------------------
	// (1)  start recv: byte 02/1..14; Loss of Output           low  byte
	// (2)  save byte 01/1..14
	data_adc = PDTEMP_SPI.DATA;
	PDTEMP_SPI.DATA = 0;
	hires_temperature_buffer.e.loss_of_output_15_8 = data_adc;
	//----------------------------------------------------------------------

	wait_transmission();

	//----------------------------------------------------------------------
	// (1)  start recv: byte 03/1..14; Pump Temperature Alarm   high byte
	// (2)  save byte 02/1..14;
	data_adc = PDTEMP_SPI.DATA;
	PDTEMP_SPI.DATA = 0;
	hires_temperature_buffer.e.loss_of_output_7_0 = data_adc;
	//----------------------------------------------------------------------

	wait_transmission();

	//----------------------------------------------------------------------
	// (1)  start recv: byte 04/1..14; Pump Temperature Alarm   low  byte
	// (2)  save byte     03/1..14;
	data_adc = PDTEMP_SPI.DATA;
	PDTEMP_SPI.DATA = 0;
	hires_temperature_buffer.e.pump_temperature_alarm_15_8 = data_adc;
	//----------------------------------------------------------------------

	wait_transmission();

	//----------------------------------------------------------------------
	// (1)  start recv: byte 07/1..14;
	//           FOVS_PROTO: fiber A/A..D temperature high byte
	//           FOVS_FINAL: fiber D/A..D temperature high byte
	// (2)  save byte     04/1..14;
	// (3)  set fiber A LED ON
	data_adc = PDTEMP_SPI.DATA;
	PDTEMP_SPI.DATA = 0;
	hires_temperature_buffer.e.pump_temperature_alarm_7_0 = data_adc;
	LED_CHIP_PORT.OUTSET = LED_CHIPA_bm;
	//----------------------------------------------------------------------

	wait_transmission();

	//----------------------------------------------------------------------
	// (1)  start recv: byte 08/1..14;
	//           FOVS_PROTO: fiber A/A..D temperature low  byte
	//           FOVS_FINAL: fiber D/A..D temperature low  byte
	// (2)  save byte     07/1..14;
	data_adc = PDTEMP_SPI.DATA;
	PDTEMP_SPI.DATA = 0;
#ifndef FOVS_FINAL
	hires_temperature_buffer.e.fiberAtemp_15_8 = data_adc;
#else
	hires_temperature_buffer.e.fiberDtemp_15_8 = data_adc;
#endif
	//----------------------------------------------------------------------

	wait_transmission();

	//----------------------------------------------------------------------
	// (1)  start recv: byte 09/1..14;
	//           FOVS_PROTO: fiber A/A..D temperature high byte
	//           FOVS_FINAL: fiber A/A..D temperature high byte
	// (2)  save byte     08/1..14;
	// (3)  set LED for fiber A to OFF, fiber B to ON
	data_adc = PDTEMP_SPI.DATA;
	PDTEMP_SPI.DATA = 0;
#ifndef FOVS_FINAL
	hires_temperature_buffer.e.fiberAtemp_7_0 = data_adc;
#else
	hires_temperature_buffer.e.fiberDtemp_7_0 = data_adc;
#endif
	LED_CHIP_PORT.OUTCLR = LED_CHIPA_bm;
	LED_CHIP_PORT.OUTSET = LED_CHIPB_bm;
	//----------------------------------------------------------------------

	wait_transmission();

	//----------------------------------------------------------------------
	// (1)  start recv: byte 10/1..14;
	//           FOVS_PROTO: fiber B/A..D temperature low  byte
	//           FOVS_FINAL: fiber A/A..D temperature low  byte
	// (2)  save byte     09/1..14;
	data_adc = PDTEMP_SPI.DATA;
	PDTEMP_SPI.DATA = 0;
#ifndef FOVS_FINAL
	hires_temperature_buffer.e.fiberBtemp_15_8 = data_adc;
#else
	hires_temperature_buffer.e.fiberAtemp_15_8 = data_adc;
#endif
	//----------------------------------------------------------------------

	wait_transmission();

	//----------------------------------------------------------------------
	// (1)  start recv: byte 11/1..14;
	//           FOVS_PROTO: fiber C/A..D temperature high byte
	//           FOVS_FINAL: fiber B/A..D temperature high byte
	// (2)  save byte     10/1..14;
	// (3)  set LED for fiber B to OFF, fiber C to ON
	data_adc = PDTEMP_SPI.DATA;
	PDTEMP_SPI.DATA = 0;
#ifndef FOVS_FINAL
	hires_temperature_buffer.e.fiberBtemp_7_0 = data_adc;
#else
	hires_temperature_buffer.e.fiberAtemp_7_0 = data_adc;
#endif
	LED_CHIP_PORT.OUTCLR = LED_CHIPB_bm;
	LED_CHIP_PORT.OUTSET = LED_CHIPC_bm;
	//----------------------------------------------------------------------

	wait_transmission();

	//----------------------------------------------------------------------
	// (1)  start recv: byte 12/1..14;
	//           FOVS_PROTO: fiber C/A..D temperature low  byte
	//           FOVS_FINAL: fiber B/A..D temperature low  byte
	// (2)  save byte     11/1..14;
	data_adc = PDTEMP_SPI.DATA;
	PDTEMP_SPI.DATA = 0;
#ifndef FOVS_FINAL
	hires_temperature_buffer.e.fiberCtemp_15_8 = data_adc;
#else
	hires_temperature_buffer.e.fiberBtemp_15_8 = data_adc;
#endif
	//----------------------------------------------------------------------

	wait_transmission();

	//----------------------------------------------------------------------
	// (1)  start recv: byte 13/1..14;
	//           FOVS_PROTO: fiber D/A..D temperature high byte
	//           FOVS_FINAL: fiber C/A..D temperature high byte
	// (2)  save byte     12/1..14;
	// (3)  set LED for fiber C to OFF, fiber D to ON
	data_adc = PDTEMP_SPI.DATA;
	PDTEMP_SPI.DATA = 0;
#ifndef FOVS_FINAL
	hires_temperature_buffer.e.fiberCtemp_7_0 = data_adc;
#else
	hires_temperature_buffer.e.fiberBtemp_7_0 = data_adc;
#endif
	LED_CHIP_PORT.OUTCLR = LED_CHIPC_bm;
	LED_CHIP_PORT.OUTSET = LED_CHIPD_bm;
	//----------------------------------------------------------------------

	wait_transmission();

	//----------------------------------------------------------------------
	// (1)  start recv: byte 14/1..14;
	//           FOVS_PROTO: fiber D/A..D temperature low  byte
	//           FOVS_FINAL: fiber C/A..D temperature low  byte
	// (2)  save byte     13/1..14;
	data_adc = PDTEMP_SPI.DATA;
	PDTEMP_SPI.DATA = 0;
#ifndef FOVS_FINAL
	hires_temperature_buffer.e.fiberDtemp_15_8 = data_adc;
#else
	hires_temperature_buffer.e.fiberCtemp_15_8 = data_adc;
#endif
	//----------------------------------------------------------------------

	wait_transmission();

	//----------------------------------------------------------------------
	// (1)  save byte     14/1..14;
	// (2)  set LED for fiber D to OFF
	// (3)  finalize
	data_adc = PDTEMP_SPI.DATA;
#ifndef FOVS_FINAL
	hires_temperature_buffer.e.fiberDtemp_7_0 = data_adc;
#else
	hires_temperature_buffer.e.fiberCtemp_7_0 = data_adc;
#endif
	LED_CHIP_PORT.OUTCLR = LED_CHIPD_bm;

	if (buffer_safe) {
		volatile uint8_t *const crc = &CRC.DATAIN;
		const uint8_t *buffer = &hires_temperature_buffer.byte[0];
		for (uint8_t i = 0; i < sizeof(hires_temperature_buffer) - 2;
		     ++i) {
			LOAD_STORE_POSTINC_CRC(buffer, crc, sdcard_buf_ptr);
		}
		hires_temperature_buffer.e.crc_7_0 = CRC.CHECKSUM0;
		hires_temperature_buffer.e.crc_15_8 = CRC.CHECKSUM1;
		STORE_POSTINC(sdcard_buf_ptr,
			      hires_temperature_buffer.byte[13]);
		STORE_POSTINC(sdcard_buf_ptr,
			      hires_temperature_buffer.byte[14]);
		sdcard_buffer_overrun_safe = 0;
	} else {
		volatile uint8_t *const crc = &CRC.DATAIN;
		const uint8_t * buffer = &hires_temperature_buffer.byte[0];
		for (uint8_t i = 0; i < sizeof(hires_temperature_buffer) - 2;
		     ++i) {
			asm volatile("ld __tmp_reg__, %a[from]+  \n\t"
				     "st %a[crc], __tmp_reg__  \n\t"
				     : [from] "+e" (buffer)
				     : "0" (buffer), [crc] "e" (crc)
				     :);
		}
		hires_temperature_buffer.e.crc_7_0 = CRC.CHECKSUM0;
		hires_temperature_buffer.e.crc_15_8 = CRC.CHECKSUM1;
	}
	//----------------------------------------------------------------------

	// unselect ADCs
	PDTEMP_PORT.OUTCLR = PDTEMP_CNV_bm;
}

static inline void wait_transmission(void)
{
	do {
		asm volatile ("");
	} while (!(PDTEMP_SPI.STATUS & SPI_IF_bm));
}
