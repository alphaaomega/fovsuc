/*  tasking.h
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKING_H
#define TASKING_H

#include <inttypes.h>
#include "config.h"
#include "sdcard_record_types.h"

struct pcb_minimal {
	void (*const init_por) (void);
	void (*const handler) (void);
};

struct pcb_generic {
	void (* const init_por)(void);
	void (* const handler)(void);
	uint8_t (* const is_stopped)(void);
	void (* const stop)(void);
	void (* const resume)(void);
};

struct pcb_selftest {
	void (* const init_por)(void);
	void (* handler)(void);
	uint8_t (* const is_stopped)(void);
	void (* const stop)(void);
	void (* const resume)(void);

	uint8_t (* const is_selftest)(void);
	void (* const selftest)(void);
	uint8_t (* const is_health_unknown)(void);
	uint8_t (* const is_health_healthy)(void);
	uint8_t (* const is_health_degraded)(void);
	void (* const reset_health)(void);
};

struct pcb_sdcard {
	void (* const init_por)(void);
	void (* const handler)(void);
	uint8_t (* const is_stopped)(void);
	void (* const stop)(void);
	void (* const resume)(void);

	uint8_t (* const is_selftest)(void);
	void (* const selftest)(void);
	uint8_t (* const is_health_unknown)(void);
	uint8_t (* const is_health_healthy)(void);
	uint8_t (* const is_health_degraded)(void);
	void (* const reset_health)(void);

	uint8_t (* const is_formatting)(void);
	void (* const format)(void);

	uint8_t (* const is_file_active)(void);
};

// I know you'll ignore me, but:
// do NOT use these functions directly
// USE the unified tasking struct below
void _task_dummy_init_por(void);
void _task_dummy_do_nothing(void);
static const struct pcb_minimal pcb_task_dummy = {
	.init_por = _task_dummy_init_por,
	.handler = _task_dummy_do_nothing
};

void _task_sample_init_por(void);
void _task_sample_handler(void);
static const struct pcb_minimal pcb_task_sample = {
	.init_por = _task_sample_init_por,
	.handler = _task_sample_handler
};

void _task_loader_init_por(void);
void _task_loader_handler(void);
static const struct pcb_minimal pcb_task_loader = {
	.init_por = _task_loader_init_por,
	.handler = _task_loader_handler
};

void _task_accel_fiber_init_por(void);
void _task_accel_fiber_handler(void);
uint8_t _task_accel_fiber_is_stopped(void);
void _task_accel_fiber_stop(void);
void _task_accel_fiber_resume(void);
static const struct pcb_generic pcb_task_accel_fiber = {
	.init_por = _task_accel_fiber_init_por,
	.handler = _task_accel_fiber_handler,
	.is_stopped = _task_accel_fiber_is_stopped,
	.stop = _task_accel_fiber_stop,
	.resume = _task_accel_fiber_resume
};

void _task_hires_temperature_init_por(void);
void _task_hires_temperature_handler(void);
uint8_t _task_hires_temperature_is_stopped(void);
void _task_hires_temperature_stop(void);
void _task_hires_temperature_resume(void);
static const struct pcb_generic pcb_task_hires_temperature = {
	.init_por = _task_hires_temperature_init_por,
	.handler = _task_hires_temperature_handler,
	.is_stopped = _task_hires_temperature_is_stopped,
	.stop = _task_hires_temperature_stop,
	.resume = _task_hires_temperature_resume
};

void _task_lores_temperature_init_por(void);
void _task_lores_temperature_handler(void);
uint8_t _task_lores_temperature_is_stopped(void);
void _task_lores_temperature_stop(void);
void _task_lores_temperature_resume(void);
static const struct pcb_generic pcb_task_lores_temperature = {
	.init_por = _task_lores_temperature_init_por,
	.handler = _task_lores_temperature_handler,
	.is_stopped = _task_lores_temperature_is_stopped,
	.stop = _task_lores_temperature_stop,
	.resume = _task_lores_temperature_resume
};

void _task_telemetry_init_por(void);
void _task_telemetry_handler(void);
uint8_t _task_telemetry_is_stopped(void);
void _task_telemetry_stop(void);
void _task_telemetry_resume(void);
static const struct pcb_generic pcb_task_telemetry = {
	.init_por = _task_telemetry_init_por,
	.handler = _task_telemetry_handler,
	.is_stopped = _task_telemetry_is_stopped,
	.stop = _task_telemetry_stop,
	.resume = _task_telemetry_resume
};

void _task_telecommand_init_por(void);
void _task_telecommand_handler(void);
uint8_t _task_telecommand_is_stopped(void);
void _task_telecommand_stop(void);
void _task_telecommand_resume(void);
static const struct pcb_generic pcb_task_telecommand = {
	.init_por = _task_telecommand_init_por,
	.handler = _task_telecommand_handler,
	.is_stopped = _task_telecommand_is_stopped,
	.stop = _task_telecommand_stop,
	.resume = _task_telecommand_resume
};

void _task_accel_adxl_init_por(void);
void _task_accel_adxl_handler(void);
uint8_t _task_accel_adxl_is_stopped(void);
void _task_accel_adxl_stop(void);
void _task_accel_adxl_resume(void);
uint8_t _task_accel_adxl_is_selftest(void);
void _task_accel_adxl_selftest(void);
uint8_t _task_accel_adxl_is_health_unknown(void);
uint8_t _task_accel_adxl_is_health_healthy(void);
uint8_t _task_accel_adxl_is_health_degraded(void);
void _task_accel_adxl_reset_health(void);
static const struct pcb_selftest pcb_task_accel_adxl = {
	.init_por = _task_accel_adxl_init_por,
	.handler = _task_accel_adxl_handler,
	.is_stopped = _task_accel_adxl_is_stopped,
	.stop = _task_accel_adxl_stop,
	.resume = _task_accel_adxl_resume,
	.is_selftest = _task_accel_adxl_is_selftest,
	.selftest = _task_accel_adxl_selftest,
	.is_health_unknown = _task_accel_adxl_is_health_unknown,
	.is_health_healthy = _task_accel_adxl_is_health_healthy,
	.is_health_degraded = _task_accel_adxl_is_health_degraded,
	.reset_health = _task_accel_adxl_reset_health
};

void _task_sdcard_init_por(void);
void _task_sdcard_handler(void);
uint8_t _task_sdcard_is_stopped(void);
void _task_sdcard_stop(void);
void _task_sdcard_resume(void);
uint8_t _task_sdcard_is_selftest(void);
void _task_sdcard_selftest(void);
uint8_t _task_sdcard_is_health_unknown(void);
uint8_t _task_sdcard_is_health_healthy(void);
uint8_t _task_sdcard_is_health_degraded(void);
void _task_sdcard_reset_health(void);
uint8_t _task_sdcard_is_formatting(void);
void _task_sdcard_format(void);
uint8_t _task_sdcard_is_file_active(void);
static const struct pcb_sdcard pcb_task_sdcard = {
	.init_por = _task_sdcard_init_por,
	.handler = _task_sdcard_handler,
	.is_stopped = _task_sdcard_is_stopped,
	.stop = _task_sdcard_stop,
	.resume = _task_sdcard_resume,
	.is_selftest = _task_sdcard_is_selftest,
	.selftest = _task_sdcard_selftest,
	.is_health_unknown = _task_sdcard_is_health_unknown,
	.is_health_healthy = _task_sdcard_is_health_healthy,
	.is_health_degraded = _task_sdcard_is_health_degraded,
	.reset_health = _task_sdcard_reset_health,
	.is_formatting = _task_sdcard_is_formatting,
	.is_file_active = _task_sdcard_is_file_active,
	.format = _task_sdcard_format
};

void init_shared_variables(void);
void tasking_init(void);

extern union fiber_acceleration fiber_acceleration_buffer;
extern union hires_temperature hires_temperature_buffer;
extern union lores_temperature lores_temperature_buffer;
extern union reference_acceleration reference_acceleration_buffer;

/// Multiple buffers for the SD Card (ALFAT).
///
/// There are SDCARD_NUM_CHUNK chunks of SDCARD_CHUNK_SIZE bytes. The records
/// of type union fiber_acceleration are aligned at the beginning of each chunk.
/// An empty_record is updated when switching between chunks for writing. For
/// better performance, the record is always written. To not write beyond a chunk
/// (would be the case, if no padding is needed, but the empty_record has to be
/// written somewhere) one additional byte is appended to the last chunk.
struct bigmembuffer {
	uint8_t sdcard_buffer[SDCARD_NUM_CHUNK][SDCARD_CHUNK_SIZE];
	uint8_t _one_more_byte;
};
extern struct bigmembuffer _do_not_use;
static uint8_t *const sdcard_buffer = &_do_not_use.sdcard_buffer[0][0];

extern uint8_t sdcard_read_chunk;
extern uint8_t sdcard_write_chunk;
extern uint16_t sdcard_write_chunk_position;

extern uint8_t rxsm_signal_status;
extern uint8_t sdcard_buffer_overrun_safe;
extern uint8_t filename_needs_update;
extern uint8_t filename_needs_erase;

#endif				/* TASKING_H */
