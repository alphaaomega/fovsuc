/*  sdcard_transactions.c - low layer SD Card protocol transactions
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sdcard_transactions.h"
#include "config.h"

static inline void sdcard_timer_wait(void);
static inline void sdcard_timer_restart(void);
static inline void wait_spi(void);
static inline void wait_4us(void);

static inline enum TRANSACTION_STATE wta_header0(uint8_t * const header_pos);

static inline enum TRANSACTION_STATE
wta_header1(uint8_t * const header_pos, const uint8_t size);

static inline enum TRANSACTION_STATE wta_header2(uint8_t * const header_pos);

static inline enum TRANSACTION_STATE
wta_payload_nowait(const char *payload, uint8_t * const current,
		   const uint8_t * const length);

static inline enum TRANSACTION_STATE rta_header0(uint8_t * const header_pos);

static inline enum TRANSACTION_STATE
rta_header1(uint8_t * const header_pos, const uint8_t max_length,
	    uint8_t * const actual_length);

static inline enum TRANSACTION_STATE
rta_header2(uint8_t * const header_pos, const uint8_t max_length,
	    uint8_t * const actual_length);

static inline enum TRANSACTION_STATE
rta_payload_start(char *const buffer, const uint8_t length);

static inline enum TRANSACTION_STATE rta_payload_state(void);

char rx_buffer[RX_BUFFER_SIZE];

enum TRANSACTION_STATE
sdcard_write_transaction(uint8_t * const header_pos, const char *payload,
			 uint8_t * const payload_length)
{
	static uint8_t current_payload_byte;
	if (*header_pos == 0) {
		current_payload_byte = 0;
		return wta_header0(header_pos);
	} else if (*header_pos == 1) {
		return wta_header1(header_pos, *payload_length);
	} else if (*header_pos == 2) {
		return wta_header2(header_pos);
	} else {
		// possible return values: RUNNING, FINISHED
		// leave earlier when only one byte was missing
		const enum TRANSACTION_STATE wta_res =
		    wta_payload_nowait(payload, &current_payload_byte,
				       payload_length);
		if (wta_res == FINISHED)
			return FINISHED;
		wait_4us();
		return wta_payload_nowait(payload, &current_payload_byte,
					  payload_length);
	}
}

static inline enum TRANSACTION_STATE wta_header0(uint8_t * const header_pos)
{
	SDCARD_PORT.OUTCLR = SDCARD_SS_bm;

	if (SDCARD_PORT.IN & SDCARD_BUSY_bm) {
		SDCARD_PORT.OUTSET = SDCARD_SS_bm;
		return BUSY;
	}
	SDCARD_UART_MSPI.DATA = 0x01;	// TODO maybe refactor constant
	wait_spi();
	const uint8_t ignored __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	++*header_pos;
	wait_4us();
	return RUNNING;
}

static inline enum TRANSACTION_STATE wta_header1(uint8_t * const header_pos,
						 const uint8_t size)
{
	if (SDCARD_PORT.IN & SDCARD_BUSY_bm) {
		SDCARD_PORT.OUTSET = SDCARD_SS_bm;
		return BUSY;
	}
	SDCARD_UART_MSPI.DATA = size;
	wait_spi();
	const uint8_t ready_busy_flag = SDCARD_UART_MSPI.DATA;
	if (ready_busy_flag != 0x01)
		return BUSY;
	++*header_pos;
	wait_4us();
	return RUNNING;
}

static inline enum TRANSACTION_STATE wta_header2(uint8_t * const header_pos)
{
	if (SDCARD_PORT.IN & SDCARD_BUSY_bm) {
		SDCARD_PORT.OUTSET = SDCARD_SS_bm;
		return BUSY;
	}
	SDCARD_UART_MSPI.DATA = 0;
	wait_spi();
	const uint8_t ignored __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	wait_4us();
	++*header_pos;
	return RUNNING;
}

static inline enum TRANSACTION_STATE
wta_payload_nowait(const char *const payload_start, uint8_t * const current,
		   const uint8_t * const length)
{
	if (*length == *current) {
		SDCARD_PORT.OUTSET = SDCARD_SS_bm;
		return FINISHED;
	}
	// return RUNNING because BUSY means that the transaction was aborted,
	// whereas RUNNING says that the transaction has to be continued.
	if (SDCARD_PORT.IN & SDCARD_BUSY_bm)
		return RUNNING;

	SDCARD_UART_MSPI.DATA = *(payload_start + *current);
	++*current;
	wait_spi();
	uint8_t ignored __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	if (*length == *current) {
		SDCARD_PORT.OUTSET = SDCARD_SS_bm;
		return FINISHED;
	}
	return RUNNING;
}

enum TRANSACTION_STATE sdcard_read_transaction(uint8_t * const header_pos,
					       char *const buffer,
					       const uint8_t max_length,
					       uint8_t * const actual_length)
{
	static uint8_t dma_started;

	if (*header_pos == 0) {
		dma_started = 0;
		return rta_header0(header_pos);
	} else if (*header_pos == 1) {
		return rta_header1(header_pos, max_length, actual_length);
	} else if (*header_pos == 2) {
		return rta_header2(header_pos, max_length, actual_length);
	} else {
		if (dma_started == 0) {
			dma_started = 1;
			return rta_payload_start(buffer, *actual_length);
		} else {
			return rta_payload_state();
		}
	}
}

static inline enum TRANSACTION_STATE rta_header0(uint8_t * const header_pos)
{
	SDCARD_PORT.OUTCLR = SDCARD_SS_bm;

	SDCARD_UART_MSPI.DATA = 0x02;	// TODO maybe refactor constant
	wait_spi();
	const uint8_t ignored __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	++*header_pos;
	wait_4us();
	return RUNNING;
}

static inline enum TRANSACTION_STATE rta_header1(uint8_t * const header_pos,
						 const uint8_t max_length,
						 uint8_t * const actual_length)
{
	SDCARD_UART_MSPI.DATA = max_length;
	wait_spi();
	*actual_length = SDCARD_UART_MSPI.DATA;
	++*header_pos;
	wait_4us();
	return RUNNING;
}

static inline enum TRANSACTION_STATE rta_header2(uint8_t * const header_pos,
						 const uint8_t max_length,
						 uint8_t * const actual_length)
{
	SDCARD_UART_MSPI.DATA = 0;
	wait_spi();
	uint8_t length_highbyte = SDCARD_UART_MSPI.DATA;
	if (length_highbyte != 0)
		*actual_length = max_length;
	else if (*actual_length > max_length)
		*actual_length = max_length;
	++*header_pos;
	wait_4us();
	return RUNNING;
}

static inline enum TRANSACTION_STATE rta_payload_start(char *const buffer,
						       const uint8_t length)
{
	if (length == 0) {
		SDCARD_PORT.OUTSET = SDCARD_SS_bm;
		return FINISHED;
	}

	DMA.CTRL &= ~DMA_ENABLE_bm;
	DMA.CH0.CTRLA &= ~DMA_CH_ENABLE_bm;
	DMA.CH1.CTRLA &= ~DMA_CH_ENABLE_bm;
	DMA.CTRL |= DMA_RESET_bm;
	do {
		asm volatile ("");
	} while (DMA.CTRL & DMA_RESET_bm);
	DMA.CTRL = DMA_ENABLE_bm | DMA_PRIMODE_CH01_gc;

	// DMA CH0 writes a dummy byte (0) every 4us to MSPI
	DMA.CH0.ADDRCTRL = DMA_CH_SRCDIR_FIXED_gc | DMA_CH_SRCRELOAD_NONE_gc
	    | DMA_CH_DESTDIR_FIXED_gc | DMA_CH_DESTRELOAD_NONE_gc;
	static const uint8_t nil = 0;
	DMA.CH0.SRCADDR0 = ((uint16_t) & nil) & 0xff;
	DMA.CH0.SRCADDR1 = ((uint16_t) & nil) >> 8;
	DMA.CH0.DESTADDR0 = ((uint16_t) & SDCARD_UART_MSPI.DATA) & 0xff;
	DMA.CH0.DESTADDR1 = ((uint16_t) & SDCARD_UART_MSPI.DATA) >> 8;
	DMA.CH0.TRIGSRC = DMA_CH_TRIGSRC_EVSYS_CH0_gc;
	DMA.CH0.TRFCNT = 1;
	DMA.CH0.REPCNT = length;
	DMA.CH0.CTRLA =
	    DMA_CH_REPEAT_bm | DMA_CH_SINGLE_bm | DMA_CH_BURSTLEN_1BYTE_gc;

	// CHA CH1 reads byte upon RXCIF from MSPI to memory buffer
	DMA.CH1.ADDRCTRL = DMA_CH_SRCDIR_FIXED_gc | DMA_CH_SRCRELOAD_NONE_gc
	    | DMA_CH_DESTDIR_INC_gc | DMA_CH_DESTRELOAD_NONE_gc;
	DMA.CH1.SRCADDR0 = ((uint16_t) & SDCARD_UART_MSPI.DATA) & 0xff;
	DMA.CH1.SRCADDR1 = ((uint16_t) & SDCARD_UART_MSPI.DATA) >> 8;
	DMA.CH1.DESTADDR0 = ((uint16_t) buffer) & 0xff;
	DMA.CH1.DESTADDR1 = ((uint16_t) buffer) >> 8;
	DMA.CH1.TRIGSRC = SDCARD_UART_TRIGSRC_RXC;
	DMA.CH1.TRFCNT = 1;
	DMA.CH1.REPCNT = length;
	DMA.CH1.CTRLA =
	    DMA_CH_REPEAT_bm | DMA_CH_SINGLE_bm | DMA_CH_BURSTLEN_1BYTE_gc;

	// flush UART buffers -- make sure RXCIF is cleared
	const uint8_t ignored0 __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	const uint8_t ignored1 __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	const uint8_t ignored2 __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	const uint8_t ignored3 __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;

//      sdcard_timer_wait();     // XXX maybe not needed?
	SDCARD_TIMER.PER = TIMER_7US_PERIOD;
	SDCARD_TIMER.CTRLA = TIMER_7US_CLKSEL;
	sdcard_timer_restart();

	DMA.CH0.CTRLA |= DMA_CH_ENABLE_bm;
	DMA.CH1.CTRLA |= DMA_CH_ENABLE_bm;

	return RUNNING;
}

static inline enum TRANSACTION_STATE rta_payload_state(void)
{
	if ((DMA.CH0.CTRLB & (DMA_CH_CHBUSY_bm | DMA_CH_CHPEND_bm))
	    || !(DMA.INTFLAGS & DMA_CH0TRNIF_bm))
		return RUNNING;

	if ((DMA.CH1.CTRLB & (DMA_CH_CHBUSY_bm | DMA_CH_CHPEND_bm))
	    || !(DMA.INTFLAGS & DMA_CH1TRNIF_bm))
		return RUNNING;

	// flush UART buffers
	const uint8_t ignored0 __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	const uint8_t ignored1 __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	const uint8_t ignored2 __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;
	const uint8_t ignored3 __attribute__ ((unused)) = SDCARD_UART_MSPI.DATA;

	// reset the DMA
	DMA.INTFLAGS |= DMA_CH0TRNIF_bm | DMA_CH1TRNIF_bm;
	DMA.CH0.CTRLA &= ~DMA_CH_ENABLE_bm;
	DMA.CH1.CTRLA &= ~DMA_CH_ENABLE_bm;
	DMA.CTRL &= ~DMA_ENABLE_bm;

	SDCARD_PORT.OUTSET = SDCARD_SS_bm;
	return FINISHED;
}

static inline void sdcard_timer_wait(void)
{
	do {
		asm volatile ("");
	} while (!(SDCARD_TIMER.INTFLAGS & TC0_OVFIF_bm));
}

static inline void sdcard_timer_restart(void)
{
	SDCARD_TIMER.CTRLFSET = TC_CMD_RESTART_gc;
	SDCARD_TIMER.INTFLAGS |= TC0_OVFIF_bm;
}

static inline void wait_spi(void)
{
	do {
		asm volatile ("");
	} while (!(SDCARD_UART_MSPI.STATUS & USART_RXCIF_bm));
	// XXX also check TXCIF?
}

static inline void wait_4us(void)
{
	SDCARD_TIMER.PER = TIMER_4US_PERIOD;
	SDCARD_TIMER.CTRLA = TIMER_4US_CLKSEL;
	sdcard_timer_restart();
	sdcard_timer_wait();
}
