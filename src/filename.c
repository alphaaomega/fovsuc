/*  filename.c - handling of SD Card file names for measurement data
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "filename.h"
#include "tasking.h"

#define FILENAME_BUFFER_SIZE 1024
#define FIRST_FILENAME 0
#define LAST_FILENAME 99
#define NUM_FILENAMES (LAST_FILENAME - FIRST_FILENAME + 1)
#if FILENAME_BUFFER_SIZE % NUM_FILENAMES == 0
#error FILENAME_BUFFER_SIZE must not be multiple of available NUM_FILENAMES
#endif

static inline void
memcpy_eeprom_sram(uint8_t * from_eep, uint8_t * to_sram, const uint16_t count);

/// Buffer to store the used filenames to; used to avoid wear-leveling.
static uint8_t filename_buffer[FILENAME_BUFFER_SIZE]
__attribute__((section(".eeprom")));

static uint8_t current_filename;
static uint16_t next_pos;

/// Scan through the filename buffer and select the next free name.
/// \warning The function uses the #sdcard_buffer to store the list of file
///          names to enhance performance. Afterwards the buffer can be used as
//           before, but interleaving accesses to the buffer MUST be avoided!
void search_next_free_filename(void)
{
	memcpy_eeprom_sram(filename_buffer, sdcard_buffer,
	    FILENAME_BUFFER_SIZE);

	if (sdcard_buffer[0] >= NUM_FILENAMES) {
		// first byte in buffer is a free/unprogrammed memory cell
		// its value is out of target range
		current_filename = 0;
		next_pos = 0;
		return;
	}

	uint8_t predecessor;
	uint8_t expected;
	uint8_t actual;

	uint16_t i = 1;
	while (1) {
		const uint16_t predecessor_index =
		    (i + FILENAME_BUFFER_SIZE - 1) % FILENAME_BUFFER_SIZE;
		predecessor = sdcard_buffer[predecessor_index];
		actual = sdcard_buffer[i];
		expected = predecessor + 1;
		if (expected >= NUM_FILENAMES)
			expected = 0;

		if (actual != expected) {
			current_filename = expected;
			next_pos = i;
			return;
		}
		i = (i + 1) % FILENAME_BUFFER_SIZE;
	}
}

/// Provides access to the currently free file name as packed number.
/// The filenames go from 00 to 99, the least significant number is placed in
/// the low nibble of the returned byte, the most significant number in the
/// high nibble (i.e. packed BCD).
/// \return the current file name as packed pair of numbers (packed BCD).
uint8_t get_packed_file_name(void)
{
	uint8_t ret = (current_filename / 10) & 0x0f;
	ret <<= 4;
	ret |= ((current_filename % 10) & 0x0f);
	return ret;
}

/// Writes the chosen file name (single char) back to EEPROM
/// and selects the next free file name.
/// \return non-zero on success, 0 on failure.
uint8_t use_file_name(void)
{
	if (NVM.STATUS & NVM_NVMBUSY_bm)
		return 0;

	if (NVM.STATUS & NVM_EELOAD_bm) {
		// some data is already pending in the buffer
		// erase it and try again later (return no success)
		NVM.CMD = NVM_CMD_ERASE_EEPROM_BUFFER_gc;
		CCP = CCP_IOREG_gc;
		NVM.CTRLA = NVM_CMDEX_bm;
		return 0;
	}

	NVM.CMD = NVM_CMD_LOAD_EEPROM_BUFFER_gc;
	const uint16_t addr = (uint16_t) (filename_buffer + next_pos);
	NVM.ADDR0 = (uint8_t) addr;
	NVM.ADDR1 = (uint8_t) (addr >> 8);
	NVM.ADDR2 = 0;
	NVM.DATA0 = current_filename;

	NVM.CMD = NVM_CMD_ERASE_WRITE_EEPROM_PAGE_gc;
	NVM.ADDR0 = (uint8_t) addr;
	NVM.ADDR1 = (uint8_t) (addr >> 8);
	NVM.ADDR2 = 0;
	CCP = CCP_IOREG_gc;
	NVM.CTRLA = NVM_CMDEX_bm;

	++current_filename;
	if (current_filename >= NUM_FILENAMES)
		current_filename = 0;
	next_pos = (next_pos + 1) % FILENAME_BUFFER_SIZE;
	return 1;
}

/// Erases the EEPROM and resets the current file name and pointers.
/// \return non-zero on success, 0 on failure.
uint8_t erase_filename_buffer(void)
{
	if (NVM.STATUS & NVM_NVMBUSY_bm)
		return 0;

	NVM.CMD = NVM_CMD_ERASE_EEPROM_gc;
	CCP = CCP_IOREG_gc;
	NVM.CTRLA = NVM_CMDEX_bm;
	next_pos = 0;
	current_filename = 0;
	return 1;
}

/// Copies the specified number of bytes from the EEPROM to SRAM.
/// \warning This does not use the AVR-Libc functions, as they use memory mapped
///          EEPROM access and the datasheet says:
///          "When a memory mapped EEPROM page buffer load operation is
///           performed, the CPU is halted for two cycles before the next
///           instruction is executed."
///          We do not want this. So use (slow) reading via NVM commands.
static inline void
memcpy_eeprom_sram(uint8_t * from_eep, uint8_t * to_sram, const uint16_t count)
{
	do {
		asm volatile ("");
	} while (NVM.STATUS & NVM_NVMBUSY_bm);

	for (uint16_t i = count; i > 0; --i) {
		NVM.CMD = NVM_CMD_READ_EEPROM_gc;
		NVM.ADDR0 = (uint8_t) (uint16_t) from_eep;
		NVM.ADDR1 = (uint8_t) ((uint16_t) from_eep >> 8);
		NVM.ADDR2 = 0;
		CCP = CCP_IOREG_gc;
		NVM.CTRLA = NVM_CMDEX_bm;
		*to_sram = NVM.DATA0;
		++from_eep;
		++to_sram;
	}
}
