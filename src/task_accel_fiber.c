/*  task_accel_fiber.c - task to read fiber ADC measurements
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tasking.h"
#include "common.h"

static inline void handler_powerup(void);
static inline void handler_accel_fiber(void);

/// Counter for the fiber_acceleration record.
///
/// Contains the counter value for the next fiber_acceleration record, thus,
/// it has to be incremented AFTER use (post-increment).
static union fiber_acceleration_record_counter {
	/// Bytewise access.
	uint8_t byte[4];
	/// Counter access.
	uint32_t cnt;
} fiber_accel_cnt;

static enum {
	POWERUP,
	RUNNING,
	STOPPED
} task_state;

void _task_accel_fiber_init_por(void)
{
	fiber_accel_cnt.cnt = 0;
	task_state = POWERUP;
	sdcard_buffer_overrun_safe = 0;
}

uint8_t _task_accel_fiber_is_stopped(void)
{
	return STOPPED == task_state;
}

void _task_accel_fiber_stop(void)
{
	// switch laser OFF
	LASER_PORT.OUTSET = LASER_PIN_bm;
	task_state = STOPPED;
}

void _task_accel_fiber_resume(void)
{
	// switch laser ON
	LASER_PORT.OUTCLR = LASER_PIN_bm;
	task_state = RUNNING;
}

void _task_accel_fiber_handler(void)
{
	const uint8_t ts = task_state;
	if (ts == RUNNING) {
		handler_accel_fiber();
	} else if (ts == POWERUP) {
		handler_powerup();
		task_state = RUNNING;
	} else if (ts == STOPPED) {
		++fiber_accel_cnt.cnt;
	}
}

static inline void handler_powerup(void)
{
	// count the time since poweroff, including during POWERUP
	++fiber_accel_cnt.cnt;
	// switch laser ON
	LASER_PORT.OUTCLR = LASER_PIN_bm;
}

/// Reads the fiber primary and reference ADCs (chained).
///
/// The SPI Write Collision Flag is ignored.
///
/// The fiber ADCs are connected to the daisy chain as following:
/// <pre>
/// [uC] <-- [Apri]<-[Asec]<-[Bpri]<-[Bsec]<-[Cpri]<-[Csec]<-[Dpri]<-[Dsec]
/// </pre>
static inline void handler_accel_fiber(void)
{
	LED_CHIP_PORT.OUTCLR = LED_CHIPA_bm | LED_CHIPB_bm | LED_CHIPC_bm
	    | LED_CHIPD_bm;

	//----------------------------------------------------------------------
	// (1)  omit the fiber ADC's busy flag
	// (2)  set fiber A LED ON
	PCACCEL_PORT.OUTCLR = PCACCEL_SCK_bm;
	nop(2);
	PCACCEL_PORT.OUTSET = PCACCEL_SCK_bm;
	nop(2);

	// initialize CRC module with "0" pattern (all zeros)
	CRC.CTRL = CRC_RESET_RESET0_gc;
	CRC.CTRL = CRC_SOURCE_IO_gc;

	uint8_t *sdcard_buf_ptr;
	uint8_t data_fiber;

	{
		// local copies of global variables to speed up usage
		uint8_t cur_chunk = sdcard_write_chunk;
		uint16_t chunk_pos = sdcard_write_chunk_position;
		const uint16_t avail_bytes = SDCARD_CHUNK_SIZE - chunk_pos;

		// THIS IS OPTIMIZED CODE.
		// DO NOT TOUCH UNLESS YOU KNOW EXACTLY WHAT YOU DO.
		//
		// write the 'empty record' for padding if possible.
		//
		// it is safe to write the padding byte even if we are one byte
		// behind the last byte in sdcard_buffer[][] because one
		// additional byte is is reserved.
		// (see 'union _do_not_use')
		*(sdcard_buffer + cur_chunk * SDCARD_CHUNK_SIZE + chunk_pos) =
		    (EMPTY_RECORD_ID << 5) | (((uint8_t) avail_bytes) & 0x1f);

		// THIS IS OPTIMIZED CODE.
		// DO NOT TOUCH UNLESS YOU KNOW EXACTLY WHAT YOU DO.
		//
		// select the next chunk if necessary
		// fiber_accel and hires_temperature together is the biggest
		// record pair that can be written within one measurement cycle
		// (i.e. between 2 measurements = within 3 slots)
		//
		// The code below is equivalent, but has const run time for
		// every path:
		//
		//      if (avail_bytes < (sizeof(union fiber_acceleration)
		//          + sizeof(union hires_temperature))) {
		//              chunk_pos = 0;
		//              ++cur_chunk;
		//              ++sdcard_write_read_diff;
		//      }
		//
		// force SREG update, read SREG C flag and replicate it:
		// if C is set, results in 0x01; if C cleared, results in 0x00.
		uint8_t is_lower_bool;
		asm volatile ("ldi %[bool], hi8(%[cmp_val])  \n\t"
			      "cpi %[avail_lo], lo8(%[cmp_val])  \n\t"
			      "cpc %[avail_hi], %[bool]  \n\t"
			      "in  %[bool], __SREG__  \n\t"
			      "bst %[bool], %[carrybitpos]  \n\t"
			      "clr %[bool]  \n\t"
			      "bld %[bool], 0  \n\t"
			      : [bool] "=&r" (is_lower_bool)
			      : [carrybitpos] "I"(SREG_C),
				[avail_lo] "d" ((uint8_t) avail_bytes),
				[avail_hi] "d" ((uint8_t) (avail_bytes >> 8)),
				[cmp_val] "i" (sizeof(union fiber_acceleration)
					+ sizeof(union hires_temperature))
			      : );
		cur_chunk += is_lower_bool;

		// 0x00 becomes 0xff and 0x01 becomes 0x00
		--is_lower_bool;
		union {
			struct {
				uint8_t low;
				uint8_t high;
			} s;
			uint16_t val;
		} highlow;
		highlow.s.low = is_lower_bool;
		highlow.s.high = is_lower_bool;
		chunk_pos &= highlow.val;

		// THIS IS OPTIMIZED CODE.
		// DO NOT TOUCH UNLESS YOU KNOW EXACTLY WHAT YOU DO.
		//
		// The code below is equivalent, but has const run time for
		// every path:
		//
		//      if (cur_chunk >= SDCARD_NUM_CHUNK) {
		//              cur_chunk = 0;
		//      }
		asm volatile ("cpi %[chunk], %[cmp_val]  \n\t"
			      "brlo %=f  \n\t"
			      "ldi %[chunk], 0  \n\t"
			      "%=:  \n\t"
			      : [chunk] "=d" (cur_chunk)
			      : "0"(cur_chunk),
				[cmp_val] "i"(SDCARD_NUM_CHUNK)
			      : );

		// update for next usage/task invocation
		sdcard_write_chunk_position = chunk_pos
		    + sizeof(union fiber_acceleration);
		sdcard_write_chunk = cur_chunk;
		sdcard_buffer_overrun_safe = 1;

		// force load of pointer address now
		sdcard_buf_ptr = sdcard_buffer + cur_chunk * SDCARD_CHUNK_SIZE
		    + chunk_pos;
	}

	//----------------------------------------------------------------------
	// (1)  start recv: byte 01/1..16; fiber A/A..D pri    high byte
	// (2)  set LED for fiber A to ON
	PCACCEL_SPI.DATA = 0;
	LED_CHIP_PORT.OUTSET = LED_CHIPA_bm;
	//----------------------------------------------------------------------

	const union fiber_acceleration_record_counter counter = fiber_accel_cnt;

	nop(3);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 02/1..16; fiber A/A..D pri    low  byte
	// (2)  save byte 01/1..16; fiber A/A..D pri    high byte
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberApri_15_8 = data_fiber;
	//----------------------------------------------------------------------

	uint8_t id_lo_cnt = FIBER_ACCELERATION_RECORD_ID << 5;
	id_lo_cnt |= ((uint8_t) (RXSM_CTRL.IN >> RXSM_LO_bp << 3)) & _BV(3);
	id_lo_cnt |= counter.byte[3] & (_BV(2) | _BV(1) | _BV(0));
	fiber_acceleration_buffer.e.id__reserved__lift_off__counter_26_24
	    = id_lo_cnt;

	fiber_acceleration_buffer.e.counter_23_16 = counter.byte[2];
	fiber_acceleration_buffer.e.counter_15_8 = counter.byte[1];

	//----------------------------------------------------------------------
	// (1)  start recv: byte 03/1..16; fiber A/A..D sec    high byte
	// (2)  save byte 02/1..16; fiber A/A..D pri    low  byte
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberApri_7_0 = data_fiber;
	//----------------------------------------------------------------------

	fiber_acceleration_buffer.e.counter_7_0 = counter.byte[0];
	fiber_accel_cnt.cnt = counter.cnt + 1;

	nop(1);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 04/1..16; fiber A/A..D sec    low  byte
	// (2)  save byte 03/1..16; fiber A/A..D sec    high byte
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberAsec_15_8 = data_fiber;
	//----------------------------------------------------------------------

	nop(15);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 05/1..16; fiber B/A..D pri    high byte
	// (2)  save byte 04/1..16; fiber A/A..D sec    low  byte
	// (3)  set LED for fiber A to OFF, fiber B to ON
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberAsec_7_0 = data_fiber;
	LED_CHIP_PORT.OUTCLR = LED_CHIPA_bm;
	LED_CHIP_PORT.OUTSET = LED_CHIPB_bm;
	//----------------------------------------------------------------------

	nop(10);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 06/1..16; fiber B/A..D pri    low  byte
	// (2)  save byte 05/1..16; fiber B/A..D pri    high byte
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberBpri_15_8 = data_fiber;
	//----------------------------------------------------------------------

	nop(15);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 07/1..16; fiber B/A..D sec    high byte
	// (2)  save byte 06/1..16; fiber B/A..D pri    low  byte
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberBpri_7_0 = data_fiber;
	//----------------------------------------------------------------------

	nop(15);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 08/1..16; fiber B/A..D sec    low  byte
	// (2)  save byte 07/1..16; fiber B/A..D sec    high byte
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberBsec_15_8 = data_fiber;
	//----------------------------------------------------------------------

	nop(15);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 09/1..16; fiber C/A..D pri    high byte
	// (2)  save byte 08/1..16; fiber B/A..D sec    low  byte
	// (3)  set LED for fiber B to OFF, fiber C ON
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberBsec_7_0 = data_fiber;
	LED_CHIP_PORT.OUTCLR = LED_CHIPB_bm;
	LED_CHIP_PORT.OUTSET = LED_CHIPC_bm;
	//----------------------------------------------------------------------

	volatile uint8_t *CRC_DATAIN_ptr = &CRC.DATAIN;
	uint8_t *record_ptr = &fiber_acceleration_buffer.byte[0];
	FIX_POINTER(CRC_DATAIN_ptr);
	FIX_POINTER(record_ptr);
	FIX_POINTER(sdcard_buf_ptr);

	// byte 1/1..22
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	nop(1);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 10/1..16; fiber C/A..D pri    low  byte
	// (2)  save byte 09/1..16; fiber C/A..D pri    high byte
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberCpri_15_8 = data_fiber;
	//----------------------------------------------------------------------

	// byte 2..4/1..22
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	nop(3);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 11/1..16; fiber C/A..D sec    high byte
	// (2)  save byte 10/1..16; fiber C/A..D pri    low  byte
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberCpri_7_0 = data_fiber;
	//----------------------------------------------------------------------

	// byte 5..7/1..22
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	nop(3);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 12/1..16; fiber C/A..D sec    low  byte
	// (2)  save byte 11/1..16; fiber C/A..D sec    high byte
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberCsec_15_8 = data_fiber;
	//----------------------------------------------------------------------

	// byte 8..10/1..22
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	nop(3);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 13/1..16; fiber D/A..D pri    high byte
	// (2)  save byte 12/1..16; fiber C/A..D sec    low  byte
	// (3)  set LED for fiber C to OFF, fiber D ON
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberCsec_7_0 = data_fiber;
	LED_CHIP_PORT.OUTCLR = LED_CHIPC_bm;
	LED_CHIP_PORT.OUTSET = LED_CHIPD_bm;
	//----------------------------------------------------------------------

	// byte 11..12/1..22
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	nop(2);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 14/1..16; fiber D/A..D pri    low  byte
	// (2)  save byte 13/1..16; fiber D/A..D pri    high byte
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberDpri_15_8 = data_fiber;
	//----------------------------------------------------------------------

	// byte 13..15/1..22
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	nop(3);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 15/1..16; fiber D/A..D sec    high byte
	// (2)  save byte 14/1..16; fiber D/A..D pri    low  byte
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberDpri_7_0 = data_fiber;
	//----------------------------------------------------------------------

	// byte 16..18/1..22
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	nop(3);

	//----------------------------------------------------------------------
	// (1)  start recv: byte 16/1..16; fiber D/A..D sec    low  byte
	// (2)  save byte 15/1..16; fiber D/A..D sec    high byte
	data_fiber = PCACCEL_SPI.DATA;
	PCACCEL_SPI.DATA = 0;
	fiber_acceleration_buffer.e.fiberDsec_15_8 = data_fiber;
	//----------------------------------------------------------------------

	// byte 19/1..22
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);
	nop(11);

	//----------------------------------------------------------------------
	// (1)  save byte 16/1..16; fiber D/A..D sec    low  byte
	// (2)  set LED for fiber D to OFF
	// (3)  retrieve checksum
	data_fiber = PCACCEL_SPI.DATA;
	fiber_acceleration_buffer.e.fiberDsec_7_0 = data_fiber;
	LED_CHIP_PORT.OUTCLR = LED_CHIPD_bm;
	// byte 20/1..22
	LOAD_STORE_POSTINC_CRC(record_ptr, CRC_DATAIN_ptr, sdcard_buf_ptr);

	fiber_acceleration_buffer.e.crc_7_0 = CRC.CHECKSUM0;
	fiber_acceleration_buffer.e.crc_15_8 = CRC.CHECKSUM1;
	// byte 21..22/1..22
	STORE_POSTINC(sdcard_buf_ptr, fiber_acceleration_buffer.byte[20]);
	STORE_POSTINC(sdcard_buf_ptr, fiber_acceleration_buffer.byte[21]);
	//----------------------------------------------------------------------
}
