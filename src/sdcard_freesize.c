/*  sdcard_freesize.c - command to retrieve available SD Card space
 *  Copyright (C) 2014  Nicolas Benes
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sdcard.h"
#include "sdcard_transactions.h"
#include "sdcard_helper.h"

/// The state of the internal state machine.
static enum {
	CLEAR_FIFO,
	CMD,
	RET,
	CHECK_RET,
	SIZERET,
	CHECK_SIZERET
} state;

static enum SDCARD_DIAG_BASE state_freesize_checksizeret(void);
static void cmd_callback(void);
static void busy_callback(void);
static void check_ret_success_callback(void);

/// ALFAT command to read the free bytes on the SD Card.
static const char ALFAT_FREESIZE_CMD[] = "K M:\n";

/// The location to save the number of available byte to.
static uint64_t *free_bytes;

static uint8_t header_pos;
static uint8_t total_received_bytes;

void sdcard_freesize_initsm(uint64_t * const free)
{
	free_bytes = free;
	header_pos = 0;
	total_received_bytes = 0;
	state = CLEAR_FIFO;
}

/// Send "K" command to SD Card.
enum SDCARD_DIAG_BASE sdcard_freesize(void)
{
	if (state == CLEAR_FIFO)
		return sdcard_helper_clear_fifo(&header_pos, &state, CMD);
	if (state == CMD)
		return sdcard_helper_cmd(&header_pos, ALFAT_FREESIZE_CMD,
					 sizeof(ALFAT_FREESIZE_CMD) - 1,
					 &busy_callback, &cmd_callback);
	if (state == SIZERET)
		return sdcard_helper_receive(&header_pos, &state,
					     &total_received_bytes, 22,
					     CHECK_SIZERET);
	if (state == RET)
		return sdcard_helper_receive(&header_pos, &state,
					     &total_received_bytes, 4,
					     CHECK_RET);
	if (state == CHECK_SIZERET)
		return state_freesize_checksizeret();
	return sdcard_helper_check_cmderrno(0,
					    &check_ret_success_callback,
					    SDCARD_DIAG_PROGRESS);
}

static void cmd_callback(void)
{
	total_received_bytes = 0;
	state = RET;
}

static void busy_callback(void)
{
	header_pos = 0;
	total_received_bytes = 0;
	state = CLEAR_FIFO;
}

static void check_ret_success_callback(void)
{
	header_pos = 0;
	total_received_bytes = 0;
	state = SIZERET;
}

static enum SDCARD_DIAG_BASE state_freesize_checksizeret(void)
{
	const char *const buffer = rx_buffer;
	if (!(buffer[18] == '!' && buffer[21] == '\n')) {
		sys_health.sdcard.reason = REASON_RETCODE_DELIM;
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	const uint8_t retcode = hex2int(buffer[19]) << 4 | hex2int(buffer[20]);
	if (retcode != SDCARD_CMD_RET_SUCCESS) {
		sys_health.sdcard.reason = REASON_RETCODE_VALUE;
		sys_health.sdcard.retcode = retcode;
		return SDCARD_DIAG_COMPLETED_FAIL;
	}

	if (!(buffer[0] == '$' && buffer[17] == '\n')) {
		sys_health.sdcard.reason = REASON_DATA_DELIM;
		return SDCARD_DIAG_COMPLETED_FAIL;
	}
	// caution: uncommon loop boundaries
	uint64_t fb = 0;
	for (uint64_t i = 1; i <= 16; ++i) {
		fb = (fb << 4) | hex2int(buffer[i]);
	}
	*free_bytes = fb;

	// TODO report error when too few free bytes?

	return SDCARD_DIAG_COMPLETED_SUCCESS;
}
